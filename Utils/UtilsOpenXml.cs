
using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Shared.Utils.OpenXml
{
    public static class ExtSpreadsheetDocument
    {
        public static WorksheetPart GetWorksheetPartByName(this SpreadsheetDocument sDocument, string _sSheetName)
        {
            var _sSheets = sDocument.WorkbookPart.Workbook
                .GetFirstChild<Sheets>()
                .Elements<Sheet>()
                .Where(_sSheet => (_sSheet.Name == _sSheetName));
            
            if ((_sSheets != null) && (_sSheets.Count() > 0))
            {
                var _sRelationshipId = _sSheets.First().Id.Value;
                return (WorksheetPart)sDocument.WorkbookPart.GetPartById(_sRelationshipId);
            }
            else
                return (null);
        }
    }
    public static class ExtWorkbookPart
    {
        public static SharedStringItem GetSharedStringItem(this WorkbookPart wWorkbookPart, int iId)
        {
            return (wWorkbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(iId));
        }
    }
    public static class ExtWorksheet
    {
        public static Row GetRow(this Worksheet wWorksheet, uint uiRowIndex)
        {
            return (wWorksheet.GetFirstChild<SheetData>().Elements<Row>().Where(r => r.RowIndex == uiRowIndex).First());
        }
        public static Cell GetCell(this Worksheet wWorksheet, uint uiRowIndex, int iColumnIndex)
        {
            return (wWorksheet.GetRow(uiRowIndex).GetCell(iColumnIndex));
        }
        public static Cell GetCell(this Worksheet wWorksheet, uint uiRowIndex, string sColumnName)
        {
            return (wWorksheet.GetRow(uiRowIndex).GetCell(sColumnName));
        }
    }
    public static class ExtRow
    {
        public static bool TryGetCell(this Row rRow, int iColumnIndex, out Cell cCell)
        {
            var _cCells = rRow.Elements<Cell>();
            if (iColumnIndex < _cCells.Count())
                cCell = rRow.Elements<Cell>().ElementAt(iColumnIndex);
            else
                cCell = null;
            return (cCell != null);
        }
        public static Cell GetCell(this Row rRow, int iColumnIndex)
        {
            return (rRow.Elements<Cell>().ElementAt(iColumnIndex));
        }
        public static Cell GetCell(this Row rRow, string sColumnName)
        {
            return (rRow.Elements<Cell>()
                .Where(c => string.Compare(c.CellReference.Value, sColumnName + rRow.RowIndex, true) == 0)
                .First());
        }
    }
    public static class ExtCell
    {
        public static string GetSharedStringValue(this Cell cCell, WorkbookPart wWorkbookPart)
        {
            if (cCell.DataType == null)
            {
                if (cCell.InnerText != null)
                    return (cCell.InnerText);
                else if (cCell.InnerXml != null)
                    return (cCell.InnerXml);
            }
            else if (cCell.DataType != CellValues.SharedString)
                ;
            else if (!Int32.TryParse(cCell.InnerText, out var _iId))
                ;
            else
            {
                var _sItem = wWorkbookPart.GetSharedStringItem(_iId);
                if (_sItem.Text != null)
                    return (_sItem.Text.Text);
                else if (_sItem.InnerText != null)
                    return (_sItem.InnerText);
                else if (_sItem.InnerXml != null)
                    return (_sItem.InnerXml);
            }
            return ("");
        }
    }
    /*
    public static class ExtSheetData
    {
        public static Cell GetCell(this SheetData sSheetData, int iRowIndex, int iColumnIndex)
        {
            return (sSheetData.Elements<Row>().ElementAt(iRowIndex).GetCell(iColumnIndex));
        }
        public static Cell GetCell(this SheetData sSheetData, int iRowIndex, string sColumnName)
        {
            return (sSheetData.Elements<Row>().ElementAt(iRowIndex).GetCell(sColumnName));
        }
    }
    */
}