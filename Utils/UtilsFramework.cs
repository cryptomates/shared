using System;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Shared.Utils.Framework
{
    #region Utilities
    public class UtlApplication
    {
        public static int GetOwnInstanceCount()
        {
            return (GetInstanceCount(System.IO.Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)));
        }
        public static int GetInstanceCount(String sProcessName)
        {
            return (GetInstances(sProcessName).Count());
        }
        public static Process[] GetInstances(String sProcessName)
        {
            return (System.Diagnostics.Process.GetProcessesByName(sProcessName));
        }
    }
    public class UtlWindow
    {
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);
        private const int WM_SETREDRAW = 11;

        public static void SetRedraw(Control ctrl, bool bEnable = true)
        {
            SendMessage(ctrl.Handle, WM_SETREDRAW, bEnable, 0);
        }
    }
    #endregion


    #region Extensions
    public static partial class ExtInvoke
    {
        // Pr�ft vor einem Invoke ob dieser wirklich erforderlich ist und f�hrt eine Aktion ggf. ohne Invoke aus.
        // > Tutorial: https://stackoverflow.com/questions/11947718/using-methodinvoker-without-invoke#11947787
        public static void InvokeEx(this Control cControl, MethodInvoker mAction)
        {
            if (cControl.InvokeRequired)
                // Invoke ausf�hren:
                cControl.Invoke(mAction);
            else
                // Aktion direkt ausf�hren:
                mAction();
        }
    }

    public static class ExtView
    {
        public static int GetSelColumn(this ListView lControl)
        {
            Debug.Assert((lControl != null), "[GSC123]");

            Point mousePos = lControl.PointToClient(Control.MousePosition);
            ListViewHitTestInfo lHitResult = lControl.HitTest(mousePos);

            return (lHitResult.Item.SubItems.IndexOf(lHitResult.SubItem));
        }
    }

    public static class ExtLinkLabel
    {
        public static void AddText(this LinkLabel lControl, string sText = "")
        {
            lControl.Text += sText + "\r\n";
        }
        public static void AddText(this LinkLabel lControl, string sText, string sLink)
        {
            int _iStart = (lControl.Text.Length - 1);

            AddText(lControl, sText);
            lControl.Links.Add(_iStart, (sText.Length + 1), sLink);
        }
        /// <summary>
        /// Adds a line of text.
        /// </summary>
        /// <param name="lControl"></param>
        /// <param name="iLinkArg">Number of passed parameter that is displayed as a link.</param>
        /// <param name="sLinkData">Data of the link.</param>
        /// <param name="sFormat">Format.</param>
        /// <param name="oArgs">Format arguments.</param>
        public static void AddText(this LinkLabel lControl, int iLinkArg, string sLinkData, string sFormat, params object[] oArgs)
        {
            // Set link:
            int _iLinkIdx = sFormat.IndexOf("{" + iLinkArg + "}");
            if (_iLinkIdx != -1)
            {
                // Get argument that will be used as link:
                object _oArg = oArgs.ElementAt(iLinkArg);
                if (_oArg != null)
                {
                    if (lControl.Text.Length > 0)
                        _iLinkIdx += lControl.Text.Length;

                    lControl.Links.Add(_iLinkIdx, _oArg.ToString().Length, sLinkData);
                }
            }

            // Insert plain text:
            AddText(lControl, string.Format(sFormat, oArgs));
        }
        /// <summary>
        /// Adds a line of text. The first argument is used as link.
        /// </summary>
        /// <param name="lControl"></param>
        /// <param name="sLinkData">Data of the link.</param>
        /// <param name="sFormat">Format.</param>
        /// <param name="oArgs">Format arguments.</param>
        public static void AddText(this LinkLabel lControl, string sLinkData, string sFormat, params object[] oArgs)
        {
            AddText(lControl, 0, sLinkData, sFormat, oArgs);
        }
    }
    #endregion
}