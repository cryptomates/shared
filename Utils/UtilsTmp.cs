// This utilities are temporarily until "Newtonsoft.Json" will be removed.
using System;
using System.Diagnostics;
using System.Globalization;
using Newtonsoft.Json.Linq;


namespace Shared.Utils.Core
{
    public class UtlParse
    {
        public static bool String(dynamic dValue, out ulong ulResult)
        {
            String sTemp;
            if (String(dValue, out sTemp))
            {
                ulResult = ulong.Parse(sTemp);
                return (true);
            }
            else
            {
                ulResult = 0;
                return (false);
            }
        }
        public static bool String(dynamic dValue, out String sResult)
        {
            if ((dValue != null) && (((JToken)dValue).Type == JTokenType.String))
            {
                sResult = dValue;
                return (true);
            }
            else
            {
                sResult = "";
                return (false);
            }
        }
        public static bool Ulong(dynamic dValue, out ulong ulResult)
        {
            if ((dValue != null) && (((JToken)dValue).Type == JTokenType.Integer))
            {
                ulResult = dValue;
                return (true);
            }
            else
            {
                ulResult = 0;
                return (false);
            }
        }
        public static bool Int(dynamic dValue, out int iResult)
        {
            if ((dValue != null) && (((JToken)dValue).Type == JTokenType.Integer))
            {
                iResult = dValue;
                return (true);
            }
            else
            {
                iResult = 0;
                return (false);
            }
        }
        public static double Double(dynamic dValue)
        {
            double dResult;
            Double(dValue, out dResult);
            return (dResult);
        }
        public static bool Double(dynamic dValue, out double dResult)
        {
            if (dValue != null)
            {
                Type tType = dValue.GetType();

                if (tType == typeof(String))
                {
                    try
                    {
                        dResult = double.Parse(dValue, CultureInfo.CurrentCulture);
                        return (true);
                    }
                    catch (System.FormatException)
                    {
                    }
                }
                else
                {
                    switch (((JToken)dValue).Type)
                    {
                        case JTokenType.Integer:
                        case JTokenType.Float:
                        case JTokenType.String:
                            dResult = dValue;
                            return (true);
                    }
                }
            }

            dResult = 0;
            return (false);
        }
        public static string ToString(double dValue)
        {
            return (dValue.ToString().Replace(',', '.'));
        }
    }
    public static class ExtJson
    {
        public static dynamic ParseValue(this JObject jObject, dynamic dKey, dynamic dDefaultVal)
        {
            Debug.Assert((jObject != null), "[PV544]");
            dynamic dValue = jObject[dKey];
            if (dValue == null)
                return (dDefaultVal);
            else
                return (dValue);
        }
        public static dynamic ParseValue(this JToken jToken, dynamic dKey, dynamic dDefaultVal)
        {
            Debug.Assert((jToken != null), "[PV544]");
            dynamic dValue = jToken[dKey];
            if (dValue == null)
                return (dDefaultVal);
            else
                return (dValue);
        }
    }
}