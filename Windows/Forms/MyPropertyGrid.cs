﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Shared.Utils.Core;

namespace Shared.Windows.Forms
{
    // Label
    // -------------------------------------------------------------------------------------------------------------
    public static class PropertyGridLabel
    {
        public const String FIELD_LABEL_WIDTH = "labelWidth";


        public static int GetSplitterWidth(PropertyGrid pControl)
        {
            if (GetHandleInfo(pControl, FIELD_LABEL_WIDTH))
                return ((int)fField.GetValue(cControl));
            else
                return (-1);
        }

        public static bool SetSplitterWidth(PropertyGrid pControl, int iWidth, bool bUsePercent = false, bool bInvalidate = true)
        {
            if (GetHandleInfo(pControl, FIELD_LABEL_WIDTH))
            {
                if (bUsePercent)
                {
                    Debug.Assert(((0 <= iWidth) && (iWidth <= 100)), "[SSW39]");

                    // Absolute Breite ermitteln:
                    iWidth = (iWidth * pControl.Width / 100);
                }

                fField.SetValue(cControl, iWidth);

                if (bInvalidate)
                    cControl.Invalidate();

                return (true);
            }
            else
                return (false);
        }


        private static bool GetHandleInfo(PropertyGrid pControl, string sField)
        {
            cControl = (Control)pControl.GetType().GetField("gridView", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(pControl);

            if (cControl != null)
            {
                fField = cControl.GetType().GetField(sField, BindingFlags.Instance | BindingFlags.NonPublic);

                return (fField != null);
            }
            else
                return (false);
        }


        private static FieldInfo fField;
        private static Control cControl;
    }


    // Property Grid Property-Functions
    // -------------------------------------------------------------------------------------------------------------
    public class PropertyGridProp
    {
        // Ermittelt sämtliche Items, die im Control 'pControl' enthalten sind.
        public static GridItemCollection GetItemCollection(PropertyGrid pControl)
        {
            object oView = pControl.GetType().GetField("gridView", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(pControl);

            return ((GridItemCollection)oView.GetType().InvokeMember("GetAllGridEntries", BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, oView, null));
        }
    }


    // Flag-Editor Property
    // -------------------------------------------------------------------------------------------------------------
    public class FlagEnumProperty
    {
        // (BETA) ...
        // - Code aufräumen!
        // - In DropDownList werden Enum-Descriptions angezeigt, aber in Property aus PropertyGrid werden die Enum-Namen angezeigt :(

        public class CheckedListBox : System.Windows.Forms.CheckedListBox
        {
            private System.ComponentModel.Container components = null;

            public CheckedListBox()
            {
                // This call is required by the Windows.Forms Form Designer.
                InitializeComponent();

                // TODO: Add any initialization after the InitForm call
            }


            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    if (components != null)
                        components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Component Designer generated code
            private void InitializeComponent()
            {
                // 
                // FlaggedCheckedListBox
                // 
                this.CheckOnClick = true;

            }
            #endregion

            // Adds an integer value and its associated description
            public CheckedListBoxItem Add(int v, string c)
            {
                CheckedListBoxItem item = new CheckedListBoxItem(v, c);
                Items.Add(item);
                return item;
            }

            public CheckedListBoxItem Add(CheckedListBoxItem item)
            {
                Items.Add(item);
                return item;
            }

            protected override void OnItemCheck(ItemCheckEventArgs e)
            {
                base.OnItemCheck(e);

                if (isUpdatingCheckStates)
                    return;

                // Get the checked/unchecked item
                CheckedListBoxItem item = Items[e.Index] as CheckedListBoxItem;

                // Update other items
                UpdateCheckedItems(item, e.NewValue);
            }

            protected void UpdateCheckedItems(int value)
            {
                isUpdatingCheckStates = true;

                if (value == 0)
                {
                    // Iterate over all items
                    for (int i = 0; i < Items.Count; i++)
                    {
                        CheckedListBoxItem item = Items[i] as CheckedListBoxItem;
                        //checked or uncheck
                        SetItemChecked(i, item.value == 0);
                    }
                }
                else
                {
                    // Iterate over all items
                    for (int i = 0; i < Items.Count; i++)
                    {
                        CheckedListBoxItem item = Items[i] as CheckedListBoxItem;

                        // If the bit for the current item is on in the bitvalue, check it
                        if ((item.value & value) == item.value && item.value != 0)
                            SetItemChecked(i, true);
                        // Otherwise uncheck it
                        else
                            SetItemChecked(i, false);
                    }
                }
                isUpdatingCheckStates = false;

            }


            // Updates items in the checklistbox
            // composite = The item that was checked/unchecked
            // cs = The check state of that item
            protected void UpdateCheckedItems(CheckedListBoxItem composite, CheckState cs)
            {

                // If the value of the item is 0, call directly.
                if (composite.value == 0)
                    UpdateCheckedItems(0);


                // Get the total value of all checked items
                int sum = 0;
                for (int i = 0; i < Items.Count; i++)
                {
                    CheckedListBoxItem item = Items[i] as CheckedListBoxItem;

                    // If item is checked, add its value to the sum.
                    if (GetItemChecked(i))
                        sum |= item.value;
                }

                // If the item has been unchecked, remove its bits from the sum
                if (cs == CheckState.Unchecked)
                    sum = sum & (~composite.value);
                // If the item has been checked, combine its bits with the sum
                else
                    sum |= composite.value;

                // Update all items in the checklistbox based on the final bit value
                UpdateCheckedItems(sum);

            }

            private bool isUpdatingCheckStates = false;

            // Gets the current bit value corresponding to all checked items
            public int GetCurrentValue()
            {
                int sum = 0;

                for (int i = 0; i < Items.Count; i++)
                {
                    CheckedListBoxItem item = Items[i] as CheckedListBoxItem;

                    if (GetItemChecked(i))
                        sum |= item.value;
                }

                return sum;
            }

            Type enumType;
            Enum enumValue;

            // Adds items to the checklistbox based on the members of the enum
            private void FillEnumMembers()
            {
                Array aValArray = Enum.GetValues(enumType);
                int nitems = 0;

                foreach (int iVal in aValArray)
                {
                    Add(iVal, UtlEnum.GetEnumDescription(enumType.GetField(Enum.GetName(enumType, iVal))));
                    nitems++;
                }

                // set drop box height to number of items * font height + 3 pix for each border space.
                this.Height = nitems * (this.Font.Height + 3);
            }

            // Checks/unchecks items based on the current value of the enum variable
            private void ApplyEnumValue()
            {
                int intVal = (int)Convert.ChangeType(enumValue, typeof(int));
                UpdateCheckedItems(intVal);

            }

            [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
            public Enum EnumValue
            {
                get
                {
                    object e = Enum.ToObject(enumType, GetCurrentValue());
                    return (Enum)e;
                }
                set
                {

                    Items.Clear();
                    enumValue = value; // Store the current enum value
                    enumType = value.GetType(); // Store enum type
                    FillEnumMembers(); // Add items for enum members
                    ApplyEnumValue(); // Check/uncheck items depending on enum value

                }
            }


        }

        public class CheckedListBoxItem
        {
            public CheckedListBoxItem(int v, string c)
            {
                value = v;
                caption = c;
            }

            public override string ToString()
            {
                return caption;
            }

            // Returns true if the value corresponds to a single bit being set
            public bool IsFlag
            {
                get
                {
                    return ((value & (value - 1)) == 0);
                }
            }

            // Returns true if this value is a member of the composite bit value
            public bool IsMemberFlag(CheckedListBoxItem composite)
            {
                return (IsFlag && ((value & composite.value) == value));
            }

            public int value;
            public string caption;
        }

        public class UIEditor : UITypeEditor
        {
            // The checklistbox
            private CheckedListBox flagEnumCB;

            public UIEditor()
            {
                flagEnumCB = new CheckedListBox();
                flagEnumCB.BorderStyle = BorderStyle.None;
            }

            public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
            {
                if (context != null
                    && context.Instance != null
                    && provider != null)
                {

                    IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));

                    if (edSvc != null)
                    {

                        Enum e = (Enum)Convert.ChangeType(value, context.PropertyDescriptor.PropertyType);
                        flagEnumCB.EnumValue = e;
                        edSvc.DropDownControl(flagEnumCB);

                        return (flagEnumCB.EnumValue);

                    }
                }
                return null;
            }

            public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
            {
                return UITypeEditorEditStyle.DropDown;
            }
        }
    }
}
