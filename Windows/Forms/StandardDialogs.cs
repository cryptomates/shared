using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Shared.Windows.Forms
{
    public class BaseStandardForm : Form
    {
        public BaseStandardForm(String sTitle)
        {
            Text = sTitle;
            ConfirmButText = "Ok";

            bButConfirm = new Button();
            bButCancel = new Button();

            bButConfirm.Text = ConfirmButText;
            bButConfirm.DialogResult = DialogResult.OK;
            bButCancel.Text = "Abbrechen";
            bButCancel.DialogResult = DialogResult.Cancel;
        }


        // Eigenschaften (Konfiguration):
        // --------------------------------------------------------------------------------------
        #region Properties.Configuration
        public String Title { set; get; }
        public String ConfirmButText { set; get; }
        #endregion


        protected Button bButConfirm;
        protected Button bButCancel;
    }


    // Dialogfeld f�r Benutzer-Eingabe.
    public class InputBox : BaseStandardForm
    {
        // Event-Handler:
        // --------------------------------------------------------------------------------------
        public delegate bool CheckUserInputEventHandler(String sInput);


        public InputBox(String sTitle, String sPromtText, String sDefaultValue = "", bool bPasswordChar = false) : base(sTitle)
        {
            PromtText = sPromtText;
            UserInput = sDefaultValue;

            label = new Label();
            textBox = new TextBox();

            if (bPasswordChar)
                textBox.PasswordChar = '*';
        }


        // Eigenschaften (Konfiguration):
        // --------------------------------------------------------------------------------------
        #region Properties.Configuration
        public String PromtText { set; get; }
        public CheckUserInputEventHandler CheckUserInputEvent { set; get; }
        #endregion
        #region Properties.Result
        public String UserInput { private set; get; }
        #endregion


        // Management:
        // --------------------------------------------------------------------------------------
        public new DialogResult Show()
        {
            int iWidth = 372;

            label.Text = PromtText;
            textBox.Text = UserInput;

            // Abmessungen f�r Pront-Text ermitteln:
            Graphics gGraphics = label.CreateGraphics();
            SizeF sPromtBounds = gGraphics.MeasureString(UserInput, label.Font, iWidth);
            int iHeight = (int)sPromtBounds.Height;

            label.SetBounds(9, 9, iWidth, 13);
            textBox.SetBounds(12, 40, iWidth, iHeight);
            bButConfirm.SetBounds(228, (56 + iHeight), 75, 23);
            bButCancel.SetBounds(309, (56 + iHeight), 75, 23);

            label.AutoSize = true;
            textBox.Anchor = (textBox.Anchor | AnchorStyles.Right);
            bButConfirm.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
            bButCancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);

            this.ClientSize = new Size(396, (91 + iHeight));
            this.Controls.AddRange(new Control[] { label, textBox, bButConfirm, bButCancel });
            this.ClientSize = new Size(Math.Max(300, label.Right + 10), this.ClientSize.Height);
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.AcceptButton = bButConfirm;
            this.CancelButton = bButCancel;

            if (CheckUserInputEvent != null)
            {
                textBox.TextChanged += OnTextChanged;

                // Event zu initialisierungszwecken ausl�sen:
                OnTextChanged(textBox, new EventArgs());
            }

            ShowDialog();
            UserInput = textBox.Text;

            return (DialogResult);
        }


        // �ffentliche Event-Handler:
        // --------------------------------------------------------------------------------------
        public static bool CheckUserInput_Number(String sInput)
        {
            int Number;

            return (int.TryParse(sInput, out Number));
        }


        // Management:
        // --------------------------------------------------------------------------------------
        public void OnTextChanged(object oSender, EventArgs eArgs)
        {
            Debug.Assert((textBox != null), "[OTC125]");
            Debug.Assert((CheckUserInputEvent != null), "[OTC126]");

            ((InputBox)textBox.Parent).bButConfirm.Enabled = CheckUserInputEvent(textBox.Text);
        }


        private Label label;
        private TextBox textBox;
    }

    // Dialogfeld f�r Eigenschaften.
    // > Nutzbar z.B. f�r Einstellungen.
    // > Tutorial: https://msdn.microsoft.com/en-us/library/aa302326.aspx
    public class PropertyDialog : BaseStandardForm
    {
        public PropertyDialog(String sTitle, object oObject) : base(sTitle)
        {
            pContent = new PropertyGrid();
            pContent.Size = new Size(300, 250);
            pContent.Dock = DockStyle.Fill;
            pContent.SelectedObject = oObject;

            this.Controls.Add(pContent);
        }


        private PropertyGrid pContent;
    }
}