using System.Collections.Generic;
using System.Reflection;

namespace System.Text.Json.Workaround
{
    #region Workarounds
    // [WORKAROUND]
    // When serializing JSON, every single JSON property will be written :(
    // Microsoft has improved this in .net 6.0.2!
    // -> https://docs.microsoft.com/en-us/dotnet/core/compatibility/serialization/6.0/timespan-serialization-format
    // Until updating .net, this is a workaround!
    [Obsolete("Converter obsolete! Please use .net 6.0.2")]
    public class TimeSpanConverter : System.Text.Json.Serialization.JsonConverter<TimeSpan>
    {
        public override TimeSpan Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) => TimeSpan.Parse(reader.GetString());
        public override void Write(Utf8JsonWriter writer, TimeSpan value, JsonSerializerOptions options) => writer.WriteStringValue(value.ToString());
    }
    #endregion
}
namespace System.Text.Json
{
    /// <summary>
    /// Specifies how to handle array elements.
    /// </summary>
    public enum ArrayElementSelectionMode
    {
        /// <summary>
        /// Do not support array elements.
        /// </summary>
        eIgnore,

        /// <summary>
        /// Selects the only element if array contains just a single element.
        /// </summary>
        eSingle,
        /// <summary>
        /// Selects the first element.
        /// </summary>
        eFirst,
        /// <summary>
        /// Selects the last element.
        /// </summary>
        eLast
    }

    public static class ExtJson
    {
        /// <summary>
        /// Writes an existing JSON string as JSON object.
        /// </summary>
        /// <param name="jElement">The element to receive a value from.</param>
        /// <param name="sPropertyName">Name of the property.</param>
        /// <param name="sData">Property data.</param>
        public static void WriteJson(this Utf8JsonWriter uWriter, string sPropertyName, string sData)
        {
            uWriter.WritePropertyName(sPropertyName);
            using (JsonDocument document = JsonDocument.Parse(sData))
            {
                document.RootElement.WriteTo(uWriter);
            }
        }
        
        /// <summary>
        /// Receives a properties value.
        /// </summary>
        /// <typeparam name="T">The properties expected type.</typeparam>
        /// <param name="jElement">The element to receive a value from.</param>
        /// <returns>The properties value.</returns>
        public static T GetValue<T>(this JsonElement jElement)
        {
            // (BETA) ...
            // Implement cases like "JsonElement.TryGetDateTimeOffset()".


            Type _tType = typeof(T);
            switch (Type.GetTypeCode(_tType))
            {
                case TypeCode.Boolean: return ((T)(object)jElement.GetBoolean());
                case TypeCode.SByte: return ((T)(object)jElement.GetSByte());
                case TypeCode.Byte: return ((T)(object)jElement.GetSByte());
                case TypeCode.Int16: return ((T)(object)jElement.GetInt16());
                case TypeCode.UInt16: return ((T)(object)jElement.GetUInt16());
                case TypeCode.Int32: return ((T)(object)jElement.GetInt32());
                case TypeCode.UInt32: return ((T)(object)jElement.GetUInt32());
                case TypeCode.Int64: return ((T)(object)jElement.GetInt64());
                case TypeCode.UInt64: return ((T)(object)jElement.GetUInt64());
                case TypeCode.Single: return ((T)(object)jElement.GetSingle());
                case TypeCode.Double: return ((T)(object)jElement.GetDouble());
                case TypeCode.Decimal: return ((T)(object)jElement.GetDecimal());
                case TypeCode.DateTime: return ((T)(object)jElement.GetDateTime());
                case TypeCode.String: return ((T)(object)jElement.GetString());

                case TypeCode.Char:
                    throw new NotImplementedException(string.Format("Failed to receive value of not implemented type '{0}' from property '{1}'!", _tType.FullName, jElement));

                case TypeCode.Empty:
                    throw new TypeLoadException(string.Format("Failed to receive empty value from property '{0}'!", jElement));

                case TypeCode.Object:
                case TypeCode.DBNull:
                    throw new TypeLoadException(string.Format("Failed to receive value of type '{0}' from property '{1}'!", _tType.FullName, jElement));

                default:
                    throw new TypeLoadException(string.Format("Unknown error on receiving value from property '{0}'!", jElement));
            }
        }

        /// <summary>
        /// Tries looking for a property path and receives the last property's value.
        /// </summary>
        /// <typeparam name="T">The properties expected type.</typeparam>
        /// <param name="jElement">The element to receive a value from.</param>
        /// <param name="tValue">The properties value (Default if property was not found).</param>
        /// <param name="sPath">The path (property names) to lookup.</param>
        /// <returns><c>true</c> if succeeded, otherwise <c>false</c>.</returns>
        public static bool TryGetPropertyValue<T>(this JsonElement jElement, out T tValue, params string[] sPath)
        {
            if (TryGetProperty(jElement, out var _jProp, sPath))
            {
                tValue = _jProp.GetValue<T>();
                return (true);
            }
            else
            {
                tValue = default(T);
                return (false);
            }
        }
        /// <summary>
        /// Tries looking for a property path and receives the last property's value.
        /// </summary>
        /// <typeparam name="T">The properties expected type.</typeparam>
        /// <param name="jElement">The element to receive a value from.</param>
        /// <param name="sPath">The path (property names) to lookup.</param>
        /// <returns>The properties value (Default if property was not found).</returns>
        public static T TryGetPropertyValue<T>(this JsonElement jElement, params string[] sPath)
        {
            TryGetPropertyValue(jElement, out T _tVal, sPath);
            return (_tVal);
        }
        /// <summary>
        /// Tries looking for a property path and receives the last property's value.
        /// </summary>
        /// <typeparam name="T">The properties expected type.</typeparam>
        /// <param name="jElement">The element to receive a value from.</param>
        /// <param name="sPath">The path (property names) to lookup.</param>
        /// <returns>The properties value.</returns>
        public static T GetPropertyValue<T>(this JsonElement jElement, params string[] sPath)
        {
            return (GetProperty(jElement, sPath).GetValue<T>());
        }

        /// <inheritdoc cref="TryGetProperty(JsonElement, out JsonElement, ArrayElementSelectionMode, string[])"/>
        public static bool TryGetProperty(this JsonElement jElement, out JsonElement jProperty, params string[] sPath) => TryGetProperty(jElement, out jProperty, ArrayElementSelectionMode.eIgnore, sPath);
        /// <summary>
        /// Looks for a property based of a path of properties.
        /// </summary>
        /// <param name="jElement">The elemnt to begin lookup.</param>
        /// <param name="jProperty">Found property.</param>
        /// <param name="aMode">Specifies how to handle arrays (if required).</param>
        /// <param name="sPath">The path (property names) to lookup.</param>
        /// <returns><c>true</c> if succeeded, otherwise <c>false</c>.</returns>
        public static bool TryGetProperty(this JsonElement jElement, out JsonElement jProperty, ArrayElementSelectionMode aMode, params string[] sPath)
        {
            jProperty = jElement;
            for (int i = 0; i < sPath.Length; i++)
            {
                if (jProperty.ValueKind == JsonValueKind.Array)
                {
                    switch (aMode)
                    {
                        case ArrayElementSelectionMode.eIgnore:
                            break;
                        case ArrayElementSelectionMode.eSingle:
                            if (jProperty.GetArrayLength() == 1)
                                jProperty = jProperty[0];
                            else
                                throw new AmbiguousMatchException("Failed to get single element!");
                            break;
                        case ArrayElementSelectionMode.eFirst:
                            jProperty = jProperty[0];
                            break;
                        case ArrayElementSelectionMode.eLast:
                            jProperty = jProperty[jProperty.GetArrayLength() - 1];
                            break;

                        default:
                            throw new NotImplementedException();
                    }
                }

                if (!jProperty.TryGetProperty(sPath[i], out jProperty))
                    return (false);
            }
            return (true);
        }
        /// <summary>
        /// Looks for a property based of a path of properties.
        /// </summary>
        /// <param name="jElement">The elemnt to begin lookup.</param>
        /// <param name="sPath">The path (property names) to lookup.</param>
        /// <returns>Found property.</returns>
        public static JsonElement GetProperty(this JsonElement jElement, params string[] sPath)
        {
            JsonElement _jProp;
            if (TryGetProperty(jElement, out _jProp, sPath))
                return (_jProp);
            else
                throw new KeyNotFoundException(string.Format("Failed to receive property '{0}'!", string.Join(", ", sPath)));
        }
        public static IDictionary<string, JsonElement> ToPathDictionary(this JsonElement jElement)
        {
            var dResult = new Dictionary<string, JsonElement>();
            ToPathDictionary(jElement, ref dResult);
            return (dResult);
        }
        private static void ToPathDictionary(JsonElement jElement, ref Dictionary<string, JsonElement> iResult, string sPrefix = "")
        {
            foreach (var _iElement in jElement.EnumerateObject())
            {
                var _sPath = _iElement.Name;
                if (!string.IsNullOrEmpty(sPrefix))
                    _sPath = (sPrefix + '.' + _sPath);

                switch (_iElement.Value.ValueKind)
                {
                    case JsonValueKind.Array:
                        int i = 0;
                        foreach (var _iField in _iElement.Value.EnumerateArray())
                            ToPathDictionary(_iField, ref iResult, string.Format("{0}[{1}]", _sPath, i++));
                        break;
                    case JsonValueKind.Object:
                        ToPathDictionary(_iElement.Value, ref iResult, _sPath);
                        break;
                    default:
                        iResult.Add(_sPath, _iElement.Value);
                        break;
                }
            }
        }
    }
}