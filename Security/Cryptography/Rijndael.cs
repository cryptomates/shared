// Tutorial: https://dotnet-snippets.de/snippet/encrypt-and-decrypt-strings/205

using System;
using System.IO;
using System.Security.Cryptography;

namespace Shared.Security.Cryptography
{
    public class RijndaelEncryption
    {
        public static String EncryptString(String sClearText, String sPassword, byte[] bSalt)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(sClearText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(sPassword, bSalt);
            byte[] encryptedData = EncryptString(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));

            return (Convert.ToBase64String(encryptedData));
        }
        private static byte[] EncryptString(byte[] bClearText, byte[] bKey, byte[] bInitVector)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = bKey;
            alg.IV = bInitVector;
            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(bClearText, 0, bClearText.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();

            return (encryptedData);
        }
        public static String DecryptString(String sCipherText, String sPassword, byte[] bSalt)
        {
            byte[] cipherBytes = Convert.FromBase64String(sCipherText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(sPassword, bSalt);
            byte[] decryptedData = DecryptString(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));

            return (System.Text.Encoding.Unicode.GetString(decryptedData));
        }
        private static byte[] DecryptString(byte[] bCipherData, byte[] bKey, byte[] bInitVector)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = bKey;
            alg.IV = bInitVector;
            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(bCipherData, 0, bCipherData.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();

            return (decryptedData);
        }
    }
}