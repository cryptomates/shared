using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Timers;

namespace Shared.Utils.Core
{
    #region Types
    /// <summary>
    /// Dictionary mit 2 Keys und einem Value.
    /// Zugriff via "this[Tuple.Create(Key1, Key2)]".
    /// </summary>
    /// <typeparam name="Key1"></typeparam>
    /// <typeparam name="Key2"></typeparam>
    /// <typeparam name="Value"></typeparam>
    public class Dictionary2x<Key1, Key2, Value> : Dictionary<Tuple<Key1, Key2>, Value> { }
    #endregion

    #region Types.Converter
    /// <summary>
    /// Anwendung in PropertyGrid-Controls um Text-Bezeichnungen von Enumerationen darzustellen.
    /// </summary>
    public class CnvEnumDescription : EnumConverter
    {
        public CnvEnumDescription(Type type) : base(type)
        {
            tEnumType = type;
        }
        public override bool CanConvertTo(ITypeDescriptorContext tContext, Type tDestType)
        {
            return (tDestType == typeof(string));
        }
        public override object ConvertTo(ITypeDescriptorContext tContext, CultureInfo cInfo, object oValue, Type tDestType)
        {
            return (UtlEnum.GetEnumDescription(tEnumType.GetField(System.Enum.GetName(tEnumType, oValue))));
        }
        public override bool CanConvertFrom(ITypeDescriptorContext tContext, Type tSrcType)
        {
            return (tSrcType == typeof(string));
        }
        public override object ConvertFrom(ITypeDescriptorContext tContext, CultureInfo cInfo, object oValue)
        {
            return (UtlEnum.GetEnumValue((String)oValue, tEnumType));
        }


        private Type tEnumType;
    }
    public abstract class CnvBooleanLabel : BooleanConverter
    {
        // Common used Implementations:
        // -------------------------------------------------------------------------------------------------------------
        public class NoYes : CnvBooleanLabel { public NoYes() : base("Nein", "Ja") { } }
        public class OffOn : CnvBooleanLabel { public OffOn() : base("Aus", "Ein") { } }
        public class InActive : CnvBooleanLabel { public InActive() : base("Inaktiv", "Aktiv") { } }
        public class DisEnabled : CnvBooleanLabel { public DisEnabled() : base("Deaktiviert", "Aktiviert") { } }
        public class InValid : CnvBooleanLabel { public InValid() : base("Ungültig", "Gültig") { } }
        public class BlockedReleased : CnvBooleanLabel { public BlockedReleased() : base("Gesperrt", "Freigegeben") { } }
        public class InSufficient : CnvBooleanLabel { public InSufficient() : base("Nicht ausreichend", "Ausreichend") { } }


        // Management:
        // -------------------------------------------------------------------------------------------------------------
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value is bool && destinationType == typeof(string))
            {
                return sText[(bool)value ? 1 : 0];
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            string txt = value as string;

            if (sText[0] == txt) return false;
            if (sText[1] == txt) return true;

            return base.ConvertFrom(context, culture, value);
        }


        protected CnvBooleanLabel(String sTextFalse, String sTextTrue)
        {
            sText = new string[] { sTextFalse, sTextTrue };
        }


        private string[] sText;
    }
    /// <summary>
    /// Expandable Object-Converter der keinen Wert bei verschachtelnden Objekten anzeigt.
    /// </summary>
    public class CnvEmptyExpandableObject : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext iContext, System.Globalization.CultureInfo cCulture, object oValue, Type tType)
        {
            if (tType == typeof(string))
                return ("");
            else
                return (base.ConvertTo(iContext, cCulture, oValue, tType));
        }
    }
    /// <summary>
    /// Expandable List-Converter der keinen Wert bei Listen anzeigt.
    /// </summary>
    public class CnvEmptyExpandableList : CnvEmptyExpandableObject
    {
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return (true);
        }
        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext iContext, object oValue, Attribute[] aAttributes)
        {
            IList lList = oValue as IList;
            if ((lList == null) || (lList.Count == 0))
                return (null);
            else
            {
                PropertyDescriptorCollection pItems = new PropertyDescriptorCollection(null);
                for (int i = 0; i < lList.Count; i++)
                {
                    DscrMetaData _mObj = (lList[i] as DscrMetaData);
                    if ((_mObj == null) || (_mObj.IsBrowsable))
                        pItems.Add(new DscrExpandableList(lList, i, string.Format("{0}.", (i + 1))));
                }
                return (pItems);
            }
        }
    }
    /// <summary>
    /// Object-Converter der ein String-Array in einer Combobox zur Auswahl stellt.
    /// </summary>
    public abstract class CnvStringList : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext iContext)
        {
            return (true); // ComboBox anwenden.
        }
        public override bool GetStandardValuesExclusive(ITypeDescriptorContext iContext)
        {
            return (true); // DropDown-Liste anwenden.
        }


        public abstract override StandardValuesCollection GetStandardValues(ITypeDescriptorContext iContext);


        #region Management
        protected object GetContextObject(ITypeDescriptorContext iContext, Type tExpectedType = null)
        {
            if ((iContext != null) && (iContext.Instance != null))
            {
                if ((tExpectedType != null) && (iContext.Instance.GetType() != tExpectedType))
                    Debug.Assert(false, "[GCO231] Unerwarteter Component-Typ!");
                else
                    return (iContext.Instance);
            }
            return (null);
        }
        #endregion
    }
    #endregion
    #region Types.Descriptor
    /// <summary>
    /// Beschreibt darzustellende Objekte mit 'MetaDescription' (falls verfügbar) oder mit Index-Informationen.
    /// </summary>
    public class DscrExpandableList : PropertyDescriptor
    {
        public DscrExpandableList(IList iList, int iIndex, string sName) : base(sName, null)
        {
            this.iList = iList;
            this.Index = iIndex;
            this.aInfo = (iList[Index] as DscrMetaData);
        }


        #region Properties.Management
        public override Type ComponentType { get { return iList.GetType(); } }
        public override bool IsReadOnly { get { return (iList.IsReadOnly); } }
        public override Type PropertyType { get { return iList[Index].GetType(); } }
        public override AttributeCollection Attributes { get { return new AttributeCollection(null); } }
        #endregion
        #region Properties
        public override string Name { get { return Index.ToString(CultureInfo.InvariantCulture); } }
        public override string DisplayName { get { return ((aInfo == null) ? base.DisplayName : (aInfo.DisplayName)); } }
        public override string Description { get { return ((aInfo == null) ? base.Description : (aInfo.Description)); } }
        public int Index { private set; get; }
        #endregion


        public override bool CanResetValue(object component)
        {
            return (true);
        }
        public override object GetValue(object component)
        {
            return (iList[Index]);
        }
        public override void ResetValue(object component)
        {
        }
        public override bool ShouldSerializeValue(object component)
        {
            return (true);
        }
        public override void SetValue(object component, object value)
        {
            iList[Index] = value;
        }


        private IList iList;
        private DscrMetaData aInfo;
    }
    public class DscrValuePropertyDescriptor : PropertyDescriptor
    {
        public DscrValuePropertyDescriptor(String sDisplayName, object oValue, bool bIsReadOnly = true) : base(sDisplayName, null)
        {
            Debug.Assert((oValue != null), "DVPD266");

            this.Value = oValue;
            this.bIsReadOnly = bIsReadOnly;
        }


        #region Properties.Management
        public override Type ComponentType { get { return Value.GetType(); } }
        public override bool IsReadOnly { get { return (bIsReadOnly); } }
        private bool bIsReadOnly;
        public override Type PropertyType { get { return (Value.GetType()); } }
        #endregion
        #region Properties
        public object Value { private set; get; }
        #endregion


        public override bool CanResetValue(object component)
        {
            return (false);
        }
        public override object GetValue(object component)
        {
            return (Value);
        }
        public override void ResetValue(object component)
        {
        }
        public override bool ShouldSerializeValue(object component)
        {
            return (true);
        }
        public override void SetValue(object component, object value)
        {
        }
    }
    /// <summary>
    /// Beschreibende Informationen für Listen-Objekte.
    /// </summary>
    public class DscrMetaData
    {
        public DscrMetaData(String sDisplayName, String sDescription = "")
        {
            DisplayName = sDisplayName;
            Description = sDescription;
        }


        #region Properties.Management
        [Browsable(false)]
        public virtual bool IsBrowsable { get { return (true); } }

        [Browsable(false)]
        public String DisplayName { protected set; get; }
        [Browsable(false)]
        public String Description { protected set; get; }
        #endregion
    }
    #endregion


    #region Utilities
    internal class UtilAssembly
    {
        public static string GetLocation(Assembly aAssembly = null)
        {
            if (aAssembly == null)
                aAssembly = Assembly.GetExecutingAssembly();
            return (aAssembly.Location);
        }
        public static string GetLocationFolder(Assembly aAssembly = null)
        {
            if (aAssembly == null)
                aAssembly = Assembly.GetExecutingAssembly();
            return (Path.GetDirectoryName(GetLocation(aAssembly)));
        }
        public static string GetTitle(Assembly aAssembly = null, bool bAppendVersion = false)
        {
            FileVersionInfo fInfo = GetFileInfo(aAssembly);

            string sTitle = fInfo.Comments;
            if (string.IsNullOrEmpty(sTitle))
                sTitle = fInfo.FileDescription;

            if (bAppendVersion)
                return (string.Format("{0} v{1}", sTitle, GetVersion(aAssembly).ToString()));
            else
                return (sTitle);
        }
        public static Version GetVersion(Assembly aAssembly = null)
        {
            return (GetInfo(aAssembly).Version);
        }
        public static FileVersionInfo GetFileInfo(Assembly aAssembly = null)
        {
            if (aAssembly == null)
                aAssembly = Assembly.GetExecutingAssembly();
            return (FileVersionInfo.GetVersionInfo(aAssembly.Location));
        }
        public static AssemblyName GetInfo(Assembly aAssembly = null)
        {
            if (aAssembly == null)
                aAssembly = Assembly.GetExecutingAssembly();
            return (aAssembly.GetName());
        }

        /// <summary>
        /// Determines defined types that declear an attrute of the specified type.
        /// </summary>
        /// <typeparam name=\"TAttrib\">Conditional attribute type.</typeparam>
        /// <param name=\"aAssembly\">Assembly to query.</param>
        /// <returns>Enumeration of matching types.</returns>
        public static IEnumerable<Type> GetDefinedTypesOf<TAttrib>(Assembly aAssembly = null)
            where TAttrib : Attribute
        {
            if (aAssembly == null)
                aAssembly = Assembly.GetExecutingAssembly();
            return (aAssembly.DefinedTypes.Where(type => type.UnderlyingSystemType.TryGetCustomAttribute<TAttrib>(out _)));
        }
        /// <summary>
        /// Determines defined types that declear an attrute of the specified type to create a (Type|Attribute) map from.
        /// </summary>
        /// <inheritdoc cref=\"GetDefinedTypesOf\"/>
        /// <returns>Dictionary of matching types, grouped per type.</returns>
        public static Dictionary<Type, TAttrib> GetDefinedTypeMap<TAttrib>(Assembly aAssembly = null)
            where TAttrib : Attribute
        {
            return (GetDefinedTypesOf<TAttrib>(aAssembly).ToDictionary(
                type => type,
                type => type.GetCustomAttribute<TAttrib>()
            ));
        }
        /// <inheritdoc cref=\"GetDefinedTypeMap\"/>
        /// <param name=\"fKeySelector\">Selector to chose a attribute member that will be used as key.</param>
        /// <returns>Dictionary of matching types, grouped per chosen attribute member.</returns>
        public static Dictionary<TKey, Type> GetDefinedTypeMap<TAttrib, TKey>(Func<TAttrib, TKey> fKeySelector, Assembly aAssembly = null)
            where TAttrib : Attribute
        {
            return (GetDefinedTypesOf<TAttrib>(aAssembly).ToDictionary(
                type => fKeySelector(type.GetCustomAttribute<TAttrib>()),
                type => type
            ));
        }

        public static string ReadManifestData(string embeddedFileName) => ReadManifestData(Assembly.GetExecutingAssembly(), embeddedFileName);
        public static string ReadManifestData(Assembly assembly, string embeddedFileName)
        {
            var res = assembly
                .GetManifestResourceNames()
                .SingleOrDefault(s => s.EndsWith(embeddedFileName, StringComparison.CurrentCultureIgnoreCase));
            if (res == null)
                throw new InvalidOperationException($"Failed to load embedded resource '{embeddedFileName}'!");

            using (var stream = assembly.GetManifestResourceStream(res))
            using (var reader = new StreamReader(stream))
            {
                return (reader.ReadToEnd());
            }
        }
    }
    public class UtlArray
    {
        public static void RemoveAt<T>(ref T[] tArray, int iIndex)
        {
            int _iNewLen = (tArray.Length - 1);

            // Move elements:
            for (int i = iIndex; i < _iNewLen; i++)
                tArray[i] = tArray[i + 1];

            // Resize array:
            Array.Resize(ref tArray, _iNewLen);
        }
        public static bool Remove<T>(ref T[] tArray, T tValue)
        {
            int _iIdx = Array.IndexOf(tArray, tValue);
            if (_iIdx == -1)
                return (false);
            else
            {
                UtlArray.RemoveAt(ref tArray, _iIdx);
                return (true);
            }
        }
    }
    public class UtlEnum
    {
        public static FieldInfo GetFieldInfo(Enum eValue) => eValue.GetType().GetField(eValue.ToString());
        public static string GetEnumDescription(Enum eValue) => GetEnumDescription(GetFieldInfo(eValue));
        public static string GetEnumDescription(FieldInfo fInfo)
        {
            DescriptionAttribute[] dAttrib = (DescriptionAttribute[])fInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((dAttrib != null) && (dAttrib.Length > 0))
                return (dAttrib[0].Description);
            else
                return (fInfo.Name);
        }

        /// <summary>
        /// Ermittelt den Enum-Wert eines Textes 'sText'.
        /// </summary>
        /// <param name="sText"></param>
        /// <param name="tEnumType"></param>
        /// <param name="bExact">Beschreibungs-Property von Enumeration muss GLEICH 'sText' sein. Andernfalls muss es nur in 'sText' enthalten sein.</param>
        /// <returns></returns>
        public static int GetEnumValue(string sText, Type tEnumType, bool bExact = true)
        {
            Array aValArray = System.Enum.GetValues(tEnumType);
            string sTemp;

            foreach (int iVal in aValArray)
            {
                sTemp = GetEnumDescription(tEnumType.GetField(System.Enum.GetName(tEnumType, iVal)));

                if (bExact)
                {
                    if (sTemp == sText)
                        return (iVal);
                }
                else
                {
                    if ((sTemp != "") && (sText.Contains(sTemp)))
                        return (iVal);
                }
            }

            return (-1);
        }
        public static TAttrib GetCustomAttribute<TAttrib>(Enum eValue)
            where TAttrib : Attribute
        {
            return (UtlEnum.GetFieldInfo(eValue).GetCustomAttribute<TAttrib>());
        }
        public static bool TryGetCustomAttribute<TAttrib>(Enum eValue, out TAttrib tResult)
            where TAttrib : Attribute
        {
            return (UtlEnum.GetFieldInfo(eValue).TryGetCustomAttribute<TAttrib>(out tResult));
        }
        public static int GetEnumCount(Type tEnumType)
        {
            return (System.Enum.GetValues(tEnumType).Length);
        }

        // Ermittelt einen zufällig Wert aus den verfügbaren Enumerations-Werten des angegebenen Typs 'tType'.
        public static object GetRandom(Type tType)
        {
            if (!tType.IsEnum)
                throw new ArgumentException("[GR212] 'tType' is not an enumeration-type!");

            Array aValArray = System.Enum.GetValues(tType);

            return (aValArray.GetValue(UtlNumber.GetRandom(1, aValArray.Length) - 1));
        }
    }
    public class UtlResources
    {
        public static Stream Load(String sName)
        {
            return (Assembly.GetEntryAssembly().GetManifestResourceStream(sName));
        }
    }
    public class UtlProcess
    {
        public static string RunCommand(string sCommand, string sWorkingDirectory = null)
        {
            try
            {
                var _pStart = new ProcessStartInfo("cmd", "/c " + sCommand)
                {
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };

                if (!string.IsNullOrEmpty(sWorkingDirectory))
                    _pStart.WorkingDirectory = sWorkingDirectory;

                var _pProc = new Process()
                {
                    StartInfo = _pStart
                };
                _pProc.Start();

                var sOutput = new StringBuilder();

                _pProc.OutputDataReceived += (oSender, dArgs) => sOutput.AppendLine(dArgs.Data);
                _pProc.ErrorDataReceived += (oSender, dArgs) => sOutput.AppendLine(dArgs.Data);

                _pProc.BeginOutputReadLine();
                _pProc.BeginErrorReadLine();
                _pProc.WaitForExit();

                return sOutput.ToString();
            }
            catch (Exception objException)
            {
                return $"Error in command: {sCommand}, {objException.Message}";
            }
        }
    }
    public class UtlPath
    {
        /// <summary>
        /// Returns the absolute path from a relative path.
        /// </summary>
        /// <param name="sRelativePath"></param>
        /// <param name="sBasePath">(Working folder if empty)</param>
        /// <returns></returns>
        public static string GetAbsolutePath(string sRelativePath, string sBasePath = "")
        {
            if (string.IsNullOrEmpty(sBasePath))
                sBasePath = AppContext.BaseDirectory;

            sBasePath.TryRemoveEnd("\\", out sBasePath);

            if (sRelativePath.TryRemoveStart(".\\", out sRelativePath))
                sRelativePath = (sBasePath + "\\" + sRelativePath);
            return (sRelativePath);
        }
        /// <summary>
        /// Returns the relative path from an absolute path.
        /// </summary>
        /// <param name="sAbsolutePath"></param>
        /// <param name="sBasePath">(Working folder if empty)</param>
        /// <returns></returns>
        public static string GetRelativePath(string sAbsolutePath, string sBasePath = "")
        {
            if (string.IsNullOrEmpty(sBasePath))
                sBasePath = AppContext.BaseDirectory;

            sBasePath.TryRemoveEnd("\\", out sBasePath);

            if (sAbsolutePath.TryRemoveStart(sBasePath, out sAbsolutePath))
            {
                sAbsolutePath.TryRemoveStart("\\", out sAbsolutePath);
                sAbsolutePath = (".\\" + sAbsolutePath);
            }
            return (sAbsolutePath);
        }
    }
    public class UtlColor
    {
        // Ermittelt den Hexadezimal-String einer Farbe.
        public static String ColorToHex(Color color)
        {
            byte[] bytes = new byte[3];
            bytes[0] = color.R;
            bytes[1] = color.G;
            bytes[2] = color.B;
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return (new String(chars));
        }


        private static char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    public class UtlBoolean
    {
        public static bool Parse(string sValue)
        {
            switch (sValue.ToLower())
            {
                case "1":
                case "true":
                case "on":
                    return (true);
            }
            return (false);
        }
    }
    public class UtlChar
    {
        public const string VOWELS = "aeiouAEIOU";


        public static bool IsVowel(char cChar)
        {
            return (VOWELS.Contains(cChar));
        }
        public static bool IsConsonant(char cChar)
        {
            return (!VOWELS.Contains(cChar));
        }
    }
    public class UtlNumber
    {
        // Begrenz einen Wert indem er bei einer Grenzwertüberschreitung der entsprechende Grenzwert übernommen wird.
        public static T Limit<T>(T tValue, T tMin, T tMax) where T : IComparable<T>
        {
            if (tValue.CompareTo(tMin) < 0)
                return (tMin);

            else if (tValue.CompareTo(tMax) > 0)
                return (tMax);

            else
                return (tValue);
        }
        // Begrenz einen Wert indem dessen Restwert innerhalb des zulässigen Bereichs aufgelöst wird.
        public static T LimitEx<T>(T tMin, T tValue, T tMax) where T : IComparable<T>
        {
            try
            {
                return ((tValue % ((dynamic)tMax - (dynamic)tMin)) + tMin);
            }
            catch
            {
                return (default(T));
            }
        }

        public static bool GetRandom()
        {
            Validate();
            return ((rRandom.Next() % 2) == 1);
        }
        public static double GetRandom(double dMin = 0, double dMax = 1)
        {
            Validate();
            return ((rRandom.NextDouble() * (dMax - dMin)) + dMin);
        }
        public static int GetRandom(int iMin = 0, int iMax = 1)
        {
            Validate();
            return (rRandom.Next(iMin, iMax));
        }


        // Management:
        // --------------------------------------------------------------------------------------
        private static void Validate()
        {
            if (rRandom == null)
                rRandom = new Random(DateTime.Now.Millisecond);
        }


        private static Random rRandom;
    }

    public class UtlDebug
    {
        public static StackFrame[] GetCallStack()
        {
            StackTrace stackTrace = new StackTrace();
            return (stackTrace.GetFrames());
        }
        public static void PrintCallStack()
        {
            MethodBase mMethod;
            foreach (StackFrame sFrame in GetCallStack())
            {
                mMethod = sFrame.GetMethod();
                Console.WriteLine(String.Format("{0}::{1}()", mMethod.ReflectedType.Name, mMethod.Name));
            }
        }
        public static void WriteCallStack(String sFileName = "")
        {
            if (sFileName == "")
                sFileName = String.Format("{0}\\StackTrace.txt", AppContext.BaseDirectory);

            StreamWriter sFile = new StreamWriter(sFileName, false);
            MethodBase mMethod;

            foreach (StackFrame sFrame in GetCallStack())
            {
                mMethod = sFrame.GetMethod();
                sFile.WriteLine(String.Format("{0}::{1}()", mMethod.ReflectedType.Name, mMethod.Name));
            }
            sFile.Close();
        }
    }
    public class UtlComponentModel
    {
        #region Constants
        public const int MAX_COLUMN = 10;
        #endregion
        #region Enumerations
        public enum EvaluationResult
        {
            /// <summary>
            /// Proceed evaluation.
            /// </summary>
            eSuccess,
            /// <summary>
            /// Proceed evaluation, but skip evaluating current type.
            /// </summary>
            eSkip,
            /// <summary>
            /// Cancel evaluation.
            /// </summary>
            eCancel
        }
        private enum EvPropPathMode
        {
            eGet,
            eGetOrCreate,
            eSet
        }
        #endregion


        #region Delegates
        public delegate EvaluationResult EvaluateTypeEventHandler(PropertyInfo pInfo, String sName, int iDepth, ref Object oTag);
        public delegate EvaluationResult EvaluateObjectEventHandler(PropertyInfo pInfo, String sName, String sValue, int iDepth, ref Object oTag);
        #endregion


        #region Evaluation.Type
        public static EvaluationResult EvaluateType(Type tType, EvaluateTypeEventHandler OnEvaluate = null, Object oTag = null)
        {
            return (EvaluateType(tType, 0, OnEvaluate, oTag));
        }
        private static EvaluationResult EvaluateType(Type tType, int iDepth, EvaluateTypeEventHandler OnEvaluate = null, Object oTag = null)
        {
            String sName;
            foreach (PropertyInfo pProperty in tType.GetProperties())
            {
                if (pProperty.IsBrowsable())
                {
                    // Ergebnis ausgeben:
                    pProperty.TryGetAttributeValue<DisplayNameAttribute, string>(out sName);

                    object _oTag = oTag;
                    switch (OnEvaluate(pProperty, sName, iDepth, ref _oTag))
                    {
                        case EvaluationResult.eSkip: continue;
                        case EvaluationResult.eCancel: return (EvaluationResult.eCancel);
                    }

                    // Child-Typen auswerten:
                    if (EvaluateType(pProperty.PropertyType, (iDepth + 1), OnEvaluate, _oTag) == EvaluationResult.eCancel)
                        return (EvaluationResult.eCancel);
                }
            }

            if (tType.IsArray)
            {
                // Load and evaluate sub-type of array:
                string _sSubType;
                if (tType.FullName.TryRemoveEnd("[]", out _sSubType))
                {
                    Type _tSubType = tType.Assembly.GetType(_sSubType);
                    if (_tSubType != null)
                    {
                        if (EvaluateType(_tSubType, iDepth, OnEvaluate, oTag) == EvaluationResult.eCancel)
                            return (EvaluationResult.eCancel);
                    }
                }
            }

            return (EvaluationResult.eSuccess);
        }
        #endregion
        #region Evaluation.Object
        public static EvaluationResult EvaluateObject(object oObject, EvaluateObjectEventHandler OnEvaluate = null, Object oTag = null, Type[] tExclude = null)
        {
            if (OnEvaluate == null)
                OnEvaluate = UtlComponentModel.OnEvaluateObject;

            return (EvaluateObject(oObject, 0, OnEvaluate, oTag, tExclude));
        }
        private static EvaluationResult EvaluateObject(object oObject, int iDepth, EvaluateObjectEventHandler OnEvaluate = null, Object oTag = null, Type[] tExclude = null)
        {
            Type tType = oObject.GetType();
            Type tStringType = typeof(String);

            int _iCount, iSubDepth = (iDepth + 1);
            PropertyInfo[] pProperties = tType.GetProperties();
            ParameterInfo[] pParam;
            String sName, sValue;
            object oChild, _oTag;
            bool bIsRawType, bIsArrayType;

            foreach (PropertyInfo pProperty in pProperties)
            {
                // Prüfen ob Property verfügbar ist:
                if (pProperty.IsBrowsable())
                {
                    pParam = pProperty.GetIndexParameters();
                    bIsArrayType = (pParam.Count() > 0);
                    bIsRawType = (!pProperty.PropertyType.IsClass);
                    oChild = (bIsArrayType) ? null : pProperty.GetValue(oObject);
                    sValue = string.Empty;

                    if (oChild != null)
                    {
                        if (oChild == oObject)
                            continue; // Confused..!?
                        else if (oChild.GetType() == oObject.GetType())
                            continue; // Confused: Type has property of own type..!?
                    }
                    if ((tExclude != null) && (tExclude.Contains(pProperty.PropertyType)))
                        continue; // Skip type.
                    else if (pProperty.PropertyType == tStringType)
                    {
                        PropertyInfo pCharsProp = pProperty.PropertyType.GetProperty("Chars");
                        PropertyInfo pLengthProp = pProperty.PropertyType.GetProperty("Length");
                        if ((pCharsProp != null) && (pLengthProp != null) && (pLengthProp.PropertyType.IsAssignableFrom(typeof(Int32))))
                        {
                            object oValueObj = pProperty.GetValue(oObject);
                            if (oValueObj != null)
                            {
                                // Zeichen einzeln auslesen:
                                int iCount = (int)pLengthProp.GetValue(oValueObj, null);
                                for (int i = 0; i < iCount; i++)
                                    sValue += pCharsProp.GetValue(oValueObj, new object[] { i });
                            }
                        }
                        else
                            continue;

                        bIsRawType = true;
                    }
                    else if (bIsArrayType)
                    {
                        // Array (-ähnliches) Objekt auswerten:
                        if (oObject is IEnumerable _iEnm)
                            _iCount = _iEnm.GetCount();
                        else
                        {
                            Debug.Assert(false, "[LC157] Unbekannter Typ!");
                            return (EvaluationResult.eCancel);
                        }

                        for (int i = 0; i < _iCount; i++)
                        {
                            // Item ausgeben:
                            _oTag = oTag;
                            switch (OnEvaluate(null, String.Format("{0} {1}", pProperty.Name, (i + 1)), string.Empty, iDepth, ref _oTag))
                            {
                                case EvaluationResult.eSkip: continue;
                                case EvaluationResult.eCancel: return (EvaluationResult.eCancel);
                            }

                            if (EvaluateObject(pProperty.GetValue(oObject, new object[] { i }), iSubDepth, OnEvaluate, _oTag, tExclude) == EvaluationResult.eCancel)
                                return (EvaluationResult.eCancel);
                        }
                        return (EvaluationResult.eSuccess);
                    }
                    else
                    {
                        if (oChild == null)
                            sValue = "null";
                        else
                            sValue = ((bIsRawType) ? oChild.ToString() : "");
                    }

                    // Ergebnis ausgeben:
                    pProperty.TryGetAttributeValue<DisplayNameAttribute, string>(out sName);

                    _oTag = oTag;
                    switch (OnEvaluate(pProperty, sName, sValue, iDepth, ref _oTag))
                    {
                        case EvaluationResult.eSkip: continue;
                        case EvaluationResult.eCancel: return (EvaluationResult.eCancel);
                    }

                    if ((bIsRawType == false) && (oChild != null))
                    {
                        // Child-Objekte auswerten:
                        if (EvaluateObject(oChild, iSubDepth, OnEvaluate, _oTag, tExclude) == EvaluationResult.eCancel)
                            return (EvaluationResult.eCancel);
                    }
                }
            }

            return (EvaluationResult.eSuccess);
        }
        private static EvaluationResult OnEvaluateObject(PropertyInfo pInfo, String sName, String sValue, int iDepth, ref Object oTag)
        {
            // Objekt ausgeben:
            for (int i = 0; i < MAX_COLUMN; i++)
            {
                if (i == iDepth)
                    Console.Write(sName);
                Console.Write(";");
            }
            Console.WriteLine(sValue);

            return (EvaluationResult.eSuccess);
        }
        #endregion
        #region Evaluation.Property
        public interface IPropertyResolver
        {
            #region Properties.Management
            bool IsValid { get; }
            Type PropertyType { get; }
            #endregion


            #region Initialization
            /// <summary>
            /// Resolves an instance's property (Resolver is afterwards ready to use).
            /// </summary>
            /// <param name="oInstance">Owning instance to resolve property from.</param>
            /// <param name="sProperty">Name of desired property.</param>
            /// <returns>Instance of resolved property.</returns>
            bool ResolveProperty(object oInstance, string sName);
            #endregion
            #region Management.Property
            /// <summary>
            /// Reads a resolved property's value.
            /// </summary>
            /// <returns>Property value.</returns>
            object ReadProperty();
            /// <summary>
            /// Writes a resolved property's value.
            /// </summary>
            void WriteProperty(object oValue);
            #endregion
            #region Management.Item
            /// <summary>
            /// Try reading a resolved property's item at a specific index.
            /// </summary>
            bool TryReadItem(int iIndex, out object oResult);
            /// <summary>
            /// Writes a resolved property's item at a specific index.
            /// </summary>
            void WriteItem(int iIndex, object oValue);
            #endregion
        }
        /// <summary>
        /// Property resolver for generic types.
        /// </summary>
        private class GenericPropertyResolver : IPropertyResolver
        {
            #region Properties.Management
            public bool IsValid => ((Owner != null) && (Property != null));
            public Type PropertyType => Property.PropertyType;
            public object Owner { private set; get; }
            public PropertyInfo Property { private set; get; }
            #endregion


            #region Initialization
            public bool ResolveProperty(object oInstance, string sName)
            {
                Owner = null;
                Property = oInstance.GetType().GetProperty(sName);
                if (Property == null)
                    return (false);
                else
                {
                    Owner = oInstance;
                    return (true);
                }
            }
            #endregion
            #region Management.Property
            public object ReadProperty() => Property.GetValue(Owner);
            public void WriteProperty(object oValue) => Property.SetValue(Owner, oValue);
            #endregion
            #region Management.Item
            public bool TryReadItem(int iIndex, out object oResult) => Property.TryGetValue(Owner, out oResult, iIndex);
            public void WriteItem(int iIndex, object oValue) => Property.SetValue(Owner, oValue, iIndex);
            #endregion
        }

        /// <summary>
        /// Gets the value of an instance's property.
        /// </summary>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="oValue"></param>
        /// <returns></returns>
        public static bool TryGetValue(object oInstance, string sProperty, out object oValue) => TryGetValue(null, oInstance, sProperty, out oValue);
        /// <summary>
        /// Gets the value of an instance's property.
        /// </summary>
        /// <param name="iResolver">Property resolver.</param>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="oValue"></param>
        /// <returns></returns>
        public static bool TryGetValue(IPropertyResolver iResolver, object oInstance, string sProperty, out object oValue)
        {
            oValue = EvaluatePropertyPath(oInstance, sProperty, EvPropPathMode.eGet, iResolver);
            return (oValue != null);
        }
        /// <summary>
        /// Gets the value of an instance's property.
        /// </summary>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <returns></returns>
        public static object GetValue(object oInstance, string sProperty) => GetValue(null, oInstance, sProperty);
        /// <summary>
        /// Gets the value of an instance's property.
        /// </summary>
        /// <param name="iResolver">Property resolver.</param>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <returns></returns>
        public static object GetValue(IPropertyResolver iResolver, object oInstance, string sProperty)
        {
            object oResult = EvaluatePropertyPath(oInstance, sProperty, EvPropPathMode.eGet, iResolver);
            if (oResult == null)
                throw new TargetException("Failed to resolve property!");
            else
                return (oResult);
        }
        /// <summary>
        /// Generically creates values along the specified path of properties.
        /// </summary>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="oValue"></param>
        public static void SetValue(object oInstance, string sProperty, object oValue) => SetValue(null, oInstance, sProperty, oValue);
        /// <summary>
        /// Generically creates values along the specified path of properties.
        /// </summary>
        /// <param name="iResolver">Property resolver.</param>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="oValue"></param>
        public static void SetValue(IPropertyResolver iResolver, object oInstance, string sProperty, object oValue) => EvaluatePropertyPath(oInstance, sProperty, EvPropPathMode.eSet, iResolver, (t) => oValue);
        /// <summary>
        /// Generically creates values along the specified path of properties.
        /// </summary>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="fCreateValue">Delegate for creating a typed value.</param>
        public static void SetValue(object oInstance, string sProperty, Func<Type, object> fValue = null) => SetValue(null, oInstance, sProperty, fValue);
        /// <summary>
        /// Generically creates values along the specified path of properties.
        /// </summary>
        /// <param name="iResolver">Property resolver.</param>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="fCreateValue">Delegate for creating a typed value.</param>
        public static void SetValue(IPropertyResolver iResolver, object oInstance, string sProperty, Func<Type, object> fValue = null) => EvaluatePropertyPath(oInstance, sProperty, EvPropPathMode.eSet, iResolver, fValue);
        /// <summary>
        /// Generically creates values along the specified path of properties.
        /// </summary>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="fCreateValue">Delegate for creating a typed value.</param>
        /// <returns>Created value.</returns>
        public static object GetOrCreateValue(object oInstance, string sProperty, Func<Type, object> fCreateValue = null) => GetOrCreateValue(null, oInstance, sProperty, fCreateValue);
        /// <summary>
        /// Generically creates values along the specified path of properties.
        /// </summary>
        /// <param name="iResolver">Property resolver.</param>
        /// <param name="oInstance">Owning instance of property to determine value from.</param>
        /// <param name="sProperty">Name (or path) of desired property.</param>
        /// <param name="fCreateValue">Delegate for creating a typed value.</param>
        /// <returns>Created value.</returns>
        public static object GetOrCreateValue(IPropertyResolver iResolver, object oInstance, string sProperty, Func<Type, object> fCreateValue = null) => EvaluatePropertyPath(oInstance, sProperty, EvPropPathMode.eGetOrCreate, iResolver, fCreateValue);
        private static object EvaluatePropertyPath(object oInstance, string sProperty, EvPropPathMode eMode, IPropertyResolver iResolver = null, Func<Type, object> fCreateValue = null)
        {
            var _sParts = sProperty.Split('.');
            Type _tType = oInstance.GetType();
            int _iMax = (_sParts.Count()-1);
            object _oVal;
            string _sName;
            IPropertyResolver _iResolver;
            int _iIdx;

            if (fCreateValue == null)
                // Create new default-value:
                fCreateValue = (_tType) => Activator.CreateInstance(_tType);

            // (BETA) ...
            // Until now only one-dimensional array indices are implemented.
            // > Missing: "Array[1][2]..."

            for (int i = 0; i <= _iMax; i++)
            {
                _sName = _sParts[i].PopIndex(out _iIdx);

                // Determine property resolver:
                if ((iResolver != null) && (iResolver.ResolveProperty(oInstance, _sName)))
                    _iResolver = iResolver;
                else if (iGenResolver.ResolveProperty(oInstance, _sName))
                    _iResolver = iGenResolver;
                else
                    throw new KeyNotFoundException(string.Format("Failed to determine resolver for property '{0}'!", _sName));

                if (!_iResolver.ResolveProperty(oInstance, _sName))
                    throw new TargetException(string.Format("Failed to resolve property '{0}' with {1}!", _sName, _iResolver.GetType().Name));
                else
                {
                    var _bIsLast = (i == _iMax);
                    var _bHasIdx = (_iIdx != -1);
                    var _pPurpose = _tType.GetPurpose();
                    
                    // (Non-Indexed) value:
                    _oVal = null;
                    if ((eMode == EvPropPathMode.eSet) && (_bIsLast) && (!_bHasIdx))
                        ; // Skip.
                    else
                        _oVal = _iResolver.ReadProperty();

                    if (_oVal == null)
                    {
                        if (eMode == EvPropPathMode.eGet)
                            return (null);
                        else
                        {
                            if ((_bIsLast) && (!_bHasIdx))
                                _oVal = fCreateValue(_iResolver.PropertyType);
                            else
                                _oVal = Activator.CreateInstance(_iResolver.PropertyType);
                            _iResolver.WriteProperty(_oVal);
                        }
                    }
                    _tType = _oVal.GetType();

                    // (Indexed) value:
                    if (_bHasIdx)
                    {
                        if (_pPurpose == ExtType.Purpose.eDictionary)
                            ; // Skip.
                        else if (_tType.IsGenericType)
                        {
                            // Determine type:
                            if (_tType.GenericTypeArguments.Count() == 1)
                                _tType = _tType.GenericTypeArguments.First();
                            else
                                throw new TypeLoadException("Unexpected count of generic types!");
                        }

                        _oVal = null;
                        if ((_bIsLast) && (eMode == EvPropPathMode.eSet))
                            ; // Skip.
                        else
                            _iResolver.TryReadItem(_iIdx, out _oVal);

                        if (_oVal == null)
                        {
                            if (_bIsLast)
                                _oVal = fCreateValue(_tType);
                            else
                                _oVal = Activator.CreateInstance(_tType);
                            _iResolver.WriteItem(_iIdx, _oVal);
                        }
                        _tType = _oVal.GetType();
                    }

                    oInstance = _oVal;
                }
            }
            return (oInstance);
        }


        private static IPropertyResolver iGenResolver = new GenericPropertyResolver();
        #endregion
        #region Management
        public static bool MemberwiseCopy(object oSource, object oTarget)
        {
            if ((oSource != null) && (oTarget != null))
            {
                Type tType = oSource.GetType();
                if (tType == oTarget.GetType())
                {
                    // Sämtliche Properties kopieren:
                    FieldInfo[] fFields = tType.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                    foreach (FieldInfo fTemp in fFields)
                        fTemp.SetValue(oTarget, fTemp.GetValue(oSource));

                    return (true);
                }
            }
            return (false);
        }
        #endregion
    }
    public class UtlFormat
    {
        public abstract class BaseFormatter : IFormatProvider, ICustomFormatter
        {
            #region Management
            public virtual string Format(string format, object arg, IFormatProvider formatProvider)
            {
                // Default formating:
                if (arg is IFormattable fmt)
                    return (fmt.ToString(format, formatProvider));
                else
                    return (arg.ToString());
            }
            public object GetFormat(Type formatType)
            {
                return (formatType == typeof(ICustomFormatter)) ? this : null;
            }
            #endregion
        }
    }

    public class UtlMath
    {
        #region Enumerations
        public enum Direction
        {
            Down,
            Up
        }
        #endregion


        public static T Between<T>(T tValue, T tMin, T tMax) where T : IComparable
        {
            if (tValue.CompareTo(tMin) < 0)
                return (tMin);
            else if (tValue.CompareTo(tMax) > 0)
                return (tMax);
            else
                return (tValue);
        }
        /// Multiplies two 32-bit values and then divides the 64-bit result by a 
        /// third 32-bit value. The final result is rounded to the nearest integer.
        public static int MulDiv(int nNumber, int nNumerator, int nDenominator)
        {
            return (int)System.Math.Round((float)nNumber * nNumerator / nDenominator);
        }

        // Tiefpassfilter, erster Ordnung.
        public static double FilterTP1(double dOldValue, double dCurValue, double dFactor = 0.1)
        {
            return (((1 - dFactor) * dOldValue) + (dFactor * dCurValue));
        }

        /// <summary>
        /// Gewichteter Mittelwert.
        /// </summary>
        /// <param name="dWeightOld"></param>
        /// <param name="dWeightNew"></param>
        /// <param name="dValueOld"></param>
        /// <param name="dValueNew"></param>
        /// <returns></returns>
        public static double GetWeightedAverage(double dWeightOld, double dWeightNew, double dValueOld, double dValueNew)
        {
            dValueOld = (dWeightOld * dValueOld);
            dValueNew = (dWeightNew * dValueNew);

            return ((dValueOld + dValueNew) / (dWeightOld + dWeightNew));
        }

        /// <summary>
        /// Exponentieller Mittelwert (EMA).
        /// </summary>
        /// <param name="dOldAvgLength">Anzahl an Einzelwerten aus denen sich der alte Mittelwert bildet.</param>
        /// <param name="dOldAvgValue">Alter Mittelwert.</param>
        /// <param name="dNewValue">Neuer Wert.</param>
        /// <returns></returns>
        public static double GetExponentialAverage(double dOldAvgLength, double dOldAvgValue, double dNewValue)
        {
            // Tutorial: http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:moving_averages

            double dMultiplier = (2 / dOldAvgLength);
            return (dMultiplier * (dNewValue - dOldAvgValue) + dOldAvgValue);
        }

        /// <summary>
        /// Variabler Mittelwert (VMA).
        /// </summary>
        /// <param name="dClose"></param>
        /// <param name="dAlpha"></param>
        /// <param name="dVI"></param>
        /// <param name="dLastVMA"></param>
        /// <returns></returns>
        public static double GetVariableAverage(double dClose, double dAlpha, double dVI, double dLastVMA)
        {
            // Tutorial:    http://etfhq.com/blog/2011/02/22/variable-moving-average-vma-volatility-index-dynamic-average-vidya/
            // Formula:     VMA = (Alpha * VI * Close) + ((1 - (Alpha * VI)) * VMA[1])

            return ((dAlpha * dVI * dClose) + ((1 - (dAlpha * dVI)) * dLastVMA));
        }
        public static double GetVariableAverage(double dOldAvgLength, double dOldAvgValue, double dNewValue)
        {
            return (GetVariableAverage(dNewValue, (2 / (dOldAvgLength + 1)), (1 / dOldAvgLength), dOldAvgValue));
        }

        // Rundet eine Zahl.
        // Tutorial: https://mycsharp.de/wbb2/thread.php?postid=89907
        // Beispiele:
        // > Round(123.456, 2) = 123.46
        // > Round(123.456, 1) = 123.50
        // > Round(123.456, 0) = 123.00
        // > Round(123.456,-1) = 120.00
        // > Round(123.456,-2) = 100.00
        // > Round(123.456,-3) = 000.00
        public static double Round(double dValue, int iDigits = 0)
        {
            double f = System.Math.Pow(10, ((iDigits == 0) ? GetCommaShift(dValue) : iDigits));
            double p = (dValue * f);
            double q = System.Math.Floor(p);
            double r = (p - q);

            if (r >= 0.5d)
                q++;

            return (q / f);
        }
        /// <summary>
        /// Rundet eine Zahl in die angegebene Richtung.
        /// </summary>
        /// <param name="dValue"></param>
        /// <param name="dDirection"></param>
        /// <param name="iGain">Verstärkung der Rundung.</param>
        /// <returns></returns>
        public static double Round(double dValue, Direction dDirection = Direction.Up, int iGain = 1)
        {
            int iCount = GetCommaShift(dValue);

            if (iCount > 0)
            {
                dLastAdjust = System.Math.Pow(10, System.Math.Abs(iCount) + 1);
                dLastAdjust = (iGain / dLastAdjust);
            }
            else
            {
                dLastAdjust = System.Math.Pow(10, System.Math.Abs(iCount) - 1);
                dLastAdjust = (iGain * dLastAdjust);
            }


            if (dValue < 0)
            {
                // (BETA) ...
                // hier muss auf anzahl an nullen hinter dem komma gerundet werden
                // > also bi 0,0034 muss auf die beiden nullen gerundet werden
            }


            // (BETA) ...
            //WEITER MACHEN
            // "dLastAdjust" wird gar nicht genutzt!?


            return (RoundEx(dValue, dDirection));
        }
        // Rundet eine Zahl in die angegebene Richtung.
        // > Es wird der zuletzt (via 'Round()' ermittelte) Anpassungs-Faktor verwendet.
        public static double RoundEx(double dValue, Direction dDirection = Direction.Up)
        {
            dValue = (((int)(dValue / dLastAdjust)) * dLastAdjust);

            if (dDirection == Direction.Up)
                return (dValue + dLastAdjust);
            else
                return (dValue);
        }
        // Ermittelt die Anzahl an Stellen VOR dem Komma.
        // Tutorial: https://www.coders-online.net/40
        public static int GetDigitCount(int iNumber)
        {
            if (iNumber != 0)
            {
                double baseExp = System.Math.Log10(System.Math.Abs(iNumber));

                return (Convert.ToInt32(System.Math.Floor(baseExp) + 1));
            }
            else
                return (1);
        }
        // Ermittelt die Anzahl an Stellen NACH dem Komma.
        // Tutorial: https://dotnet-snippets.de/snippet/anzahl-der-nachkommastellen-einer-dezimalzahl-bestimmen/1583
        public static int GetDecimalsCount(decimal dValue)
        {
            return ((Decimal.GetBits((decimal)dValue)[3] & 0x0ff0000) >> 0x10);
        }

        // Berechnet die Steigung aus mehreren Y-Werten: "Linear Regression"
        // > X-Werte (x[i] == i) werden als linear steigend erwartet.
        // Tutorial: https://www.althares.com/net/useful-linear-trendline-class-in-c/
        public static double CalcSlope(List<double> lYValues, bool bUseOffset = false)
        {
            double dResult = 0;
            int iCount = lYValues.Count();
            if (iCount >= 2)
            {
                double _dX;
                double dSumProductXY = 0, dSumXSquare = 0;
                double dSumX = 0, dSumY = 0;

                for (int i = 0; i < iCount; i++)
                {
                    _dX = i;

                    dSumProductXY += (_dX * lYValues[i]);
                    dSumX += _dX;
                    dSumY += lYValues[i];
                    dSumXSquare += System.Math.Pow(_dX, 2);
                }

                try
                {
                    // Steilheit berechnen:
                    dResult = ((iCount * dSumProductXY - dSumX * dSumY) / (iCount * dSumXSquare - System.Math.Pow(dSumX, 2)));

                    if (bUseOffset)
                        // Offset berechnen:
                        dResult = ((dSumY - dResult * dSumX) / iCount);
                }
                catch (DivideByZeroException)
                {
                }
            }
            return (dResult);
        }
        public static double GetPercent(double dValue, double dMax)
        {
            return ((dValue / dMax) * 100.0);
        }


        #region Helper
        private static int GetCommaShift(double dValue)
        {
            if ((-1 < dValue) && (dValue < 1))
                return (GetDecimalsCount((decimal)dValue) - 1);
            else
                return ((GetDigitCount((int)dValue) - 1) * -1);
        }
        #endregion


        private static double dLastAdjust = 0;
    }
    public class UtlTimer
    {
        public static System.Timers.Timer CreateTimer(ElapsedEventHandler eHandler, double dInterval = 1, bool bEnabled = false)
        {
            System.Timers.Timer tTimer = new System.Timers.Timer();

            tTimer.Elapsed += eHandler;
            tTimer.Enabled = bEnabled;

            ModifyTimer(tTimer, dInterval, bEnabled);

            return (tTimer);
        }
        public static void ModifyTimer(System.Timers.Timer tTimer, double dInterval = 1, bool bEnabled = true)
        {
            if (dInterval != -1)
                tTimer.Interval = dInterval;

            tTimer.Enabled = bEnabled;
        }
    }
    public static class UtlTimeSpan
    {
        #region Enumerations
        public enum TimeUnits
        {
            [Description("s")]
            eSecond,
            [Description("m")]
            eMinute,
            [Description("h")]
            eHour,
            [Description("D")]
            eDay
        }
        #endregion


        public static TimeSpan Parse(int iValue, TimeUnits eDefault = TimeUnits.eHour)
        {
            Validate(iValue, eDefault);
            switch (eDefault)
            {
                case TimeUnits.eSecond: return new TimeSpan(0, 0, iValue);
                case TimeUnits.eMinute: return new TimeSpan(0, iValue, 0);
                case TimeUnits.eHour: return new TimeSpan(iValue, 0, 0);
                case TimeUnits.eDay: return new TimeSpan(iValue, 0, 0, 0);
            }
            throw new NotImplementedException("[Debug] Entered Unreachable code!");
        }
        public static TimeSpan Parse(string sValue, TimeUnits eDefault = TimeUnits.eHour)
        {
            if (int.TryParse(sValue, out var _iVal))
                return (Parse(_iVal, eDefault));
            else
                return (TimeSpan.Parse(sValue));
        }


        #region Helper
        private static void Validate(int iValue, TimeUnits tUnit)
        {
            bool _bValid;
            switch (tUnit)
            {
                case TimeUnits.eSecond:
                case TimeUnits.eMinute:
                    _bValid = ((0 <= iValue) && (iValue <= 60));
                    break;
                case TimeUnits.eHour:
                    _bValid = ((0 <= iValue) && (iValue < 24));
                    break;
                case TimeUnits.eDay:
                    _bValid = true;
                    break;
                default:
                    throw new NotImplementedException(string.Format("Invalid time unit '{0}' specified!", tUnit));
            }
            if (!_bValid)
                throw new FormatException(string.Format("Invalid time value '{0}{1}'specified!", iValue, UtlEnum.GetEnumDescription(tUnit)));
        }
        #endregion
    }
    public static class UtlDateTime
    {
        #region Constants
        public const short REPEAT_TYPE_COUNT = 5;

        public static readonly TimeSpan tNon2UtcDiff = (DateTime.UtcNow - DateTime.Now);                        // Beispiel: [UTC] = "01.01.2018 18:00:00", [Non UTC] = "01.01.2018 20:00:00"
        public static readonly DateTime dFirst = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        #endregion


        #region Enumerations
        public enum RepeatType
        {
            SRT_Daily = 0,
            SRT_Weekly = 1,
            SRT_Monthly = 2,
            SRT_Yearly = 3,
            SRT_Ever = 4
        }
        #endregion


        // Ermittelt die Kalenderwoche eines Datums.
        // > Siehe 'https://www.mycsharp.de/wbb2/thread.php?postid=3610173'
        public static int GetWeekOfYear(DateTime dDate)
        {
            CultureInfo cCurCulture = CultureInfo.CurrentCulture;
            Calendar cCalendar = cCurCulture.Calendar;

            // Kalenderwoche ermitteln:
            int iResult = cCalendar.GetWeekOfYear(dDate, cCurCulture.DateTimeFormat.CalendarWeekRule, cCurCulture.DateTimeFormat.FirstDayOfWeek);

            if (iResult > 52)
            {
                // Kalenderwoche ist größer als 52:
                // In diesem Fall hat 'GetWeekOfYear()' die Kalenderwoche nicht nach ISO 8601 berechnet (Montag, der 31.12.2007 wird z.B. fälschlicherweise als KW 53 berechnet).
                if (cCalendar.GetWeekOfYear(dDate, cCurCulture.DateTimeFormat.CalendarWeekRule, cCurCulture.DateTimeFormat.FirstDayOfWeek) == 2)
                    iResult = 1;
            }

            return (iResult);
        }

        // Rundet einen Wert 'dValue' in definierten Schritten 'tSteps' auf.
        // BEISPIELE:
        // > dValue = 2010/02/05 10:35:25
        //   tSteps = TimeSpan.FromMinutes(15));
        //   RESULT = 2010/02/05 10:45:00
        public static DateTime RoundUp(DateTime dValue, TimeSpan tSteps)
        {
            var modTicks = dValue.Ticks % tSteps.Ticks;
            var delta = modTicks != 0 ? tSteps.Ticks - modTicks : 0;
            return (new DateTime(dValue.Ticks + delta, dValue.Kind));
        }
        // Rundet einen Wert 'dValue' in definierten Schritten 'tSteps' ab.
        // BEISPIELE:
        // > dValue = 2010/02/05 10:35:25
        //   tSteps = TimeSpan.FromMinutes(15));
        //   RESULT = 2010/02/05 10:30:00
        public static DateTime RoundDown(DateTime dValue, TimeSpan tSteps)
        {
            var delta = dValue.Ticks % tSteps.Ticks;
            return (new DateTime(dValue.Ticks - delta, dValue.Kind));
        }
        // (BETA) ... Beschreibung: <TODO>
        // BEISPIELE:
        // > dValue = 2010/02/05 10:35:25
        //   tSteps = TimeSpan.FromMinutes(15));
        //   RESULT = 2010/02/05 10:30:00
        public static DateTime RoundToNearest(DateTime dValue, TimeSpan tSteps)
        {
            var delta = dValue.Ticks % tSteps.Ticks;
            bool roundUp = delta > tSteps.Ticks / 2;
            var offset = roundUp ? tSteps.Ticks : 0;

            return (new DateTime(dValue.Ticks + offset - delta, dValue.Kind));
        }

        public static TimeSpan SetDaylightChanges(TimeSpan tTimeStamp)
        {
            DateTime _dTime = UTCTimeStampToDateTime(tTimeStamp);
            DaylightTime dDaylight = TimeZone.CurrentTimeZone.GetDaylightChanges(_dTime.Year);

            if ((dDaylight.Start <= _dTime) && (_dTime <= dDaylight.End))
                tTimeStamp -= dDaylight.Delta;

            return (tTimeStamp);
        }

        // Wandelt einen Zeitstempel (Sekunden ab Jahr 0) in 'DateTime' um.
        public static DateTime FromUtc0Time(double dSeconds)
        {
            return ((new DateTime(TimeSpan.FromSeconds(dSeconds).Ticks)).ToLocalTime());
        }
        // Wandelt einen Zeitstempel (Sekunden ab 01.01.1970) in 'DateTime' um.
        public static DateTime FromUtc1970Time(double dSeconds)
        {
            return ((dFirst + TimeSpan.FromSeconds(dSeconds)).ToLocalTime());
        }
        // Wandelt einen 'DateTime'-Wert in einen Zeitstempel (Sekunden ab 01.01.1970) um.
        public static double ToUtc1970Time(DateTime dValue)
        {
            return Math.Floor((dValue.ToUniversalTime() - dFirst).TotalSeconds);
        }
        // Wandelt einen Zeitstempel (Zeiträume ab 01.01.1970) in 'DateTime' um.
        [Obsolete("OBSOLETE! Please use 'FromUtc1970Time()' !")]
        public static DateTime UTCTimeStampToDateTime(TimeSpan tTimeStamp, bool bCompareLocale = false)
        {
            if (bCompareLocale)
                tTimeStamp -= tNon2UtcDiff;

            return (dFirst + tTimeStamp);
        }
        // Wandelt 'DateTime' in einen Zeitstempel (Zeiträume ab 01.01.1970) um.
        public static TimeSpan DateTimeToUTCTimeStamp(DateTime dTime, bool bCompareLocale = false)
        {
            TimeSpan tResult = (dTime - dFirst);

            if (bCompareLocale)
                tResult += tNon2UtcDiff;

            return (tResult);
        }

        public static bool IsExceeded(DateTime dValue, DateTime dCompareTo, RepeatType rRepeatType = RepeatType.SRT_Daily)
        {
            switch (rRepeatType)
            {
                case RepeatType.SRT_Daily:
                    return (dValue < dCompareTo);

                case RepeatType.SRT_Weekly:
                    return ((GetWeekOfYear(dValue) != GetWeekOfYear(dCompareTo)) || (dValue.Year != dCompareTo.Year));

                case RepeatType.SRT_Monthly:
                    return ((dValue.Month != dCompareTo.Month) || (dValue.Year != dCompareTo.Year));

                case RepeatType.SRT_Yearly:
                    return (dValue.Year != dCompareTo.Year);

                case RepeatType.SRT_Ever:
                    break;

                default:
                    Debug.Assert(false, "[IE768] Ungültiger Typ!");
                    break;
            }

            return (false);
        }
    }
    public class UtlString
    {
        // Entfernt den String 'sCut' an der angegebenen Index-Position 'iIndex' im String 'sString'.
        public static bool TryCut(ref String sString, String sCut, int iIndex = 0)
        {
            if (sString.Substring(iIndex, sCut.Length) == sCut)
            {
                sString = sString.Remove(iIndex, sCut.Length);
                return (true);
            }
            else
                return (false);
        }
    }
    public class UtlType
    {
        public static object GetDefaultValue(Type tType)
        {
            if (tType.IsValueType)
                return (Activator.CreateInstance(tType));
            else
                return (null);
        }
    }
    public class UtlJson
    {
        /// JSON converter for generic lists.
        public class ListJsonConverter<TBaseType> : JsonConverter<IEnumerable<TBaseType>> where TBaseType : class
        {
            #region Management
            public override bool CanConvert(Type tTypeToConvert)
            {
                return (typeof(IEnumerable<TBaseType>).IsAssignableFrom(tTypeToConvert));
            }
            public override IEnumerable<TBaseType> Read(ref Utf8JsonReader uReader, Type tTypeToConvert, JsonSerializerOptions jOptions)
            {
                if (uReader.TokenType != JsonTokenType.StartObject)
                    throw new JsonException("R1281");
                else
                {
                    var _aAsm = Assembly.GetExecutingAssembly();
                    var _lResult = new List<TBaseType>();

                    // Parse content:
                    while (true)
                    {
                        if (!uReader.Read()) // Read key of serialized type.
                            throw new JsonException("R1291");
                        else if (uReader.TokenType == JsonTokenType.EndObject)
                            break; // Finished serialization.
                        else if (uReader.TokenType != JsonTokenType.PropertyName)
                            throw new JsonException("R1295");
                        else
                        {
                            // Determine type:
                            var _tType = _aAsm.GetType(uReader.GetString());

                            if (!typeof(TBaseType).IsAssignableFrom(_tType))
                                throw new JsonException("R1302");
                            else
                                _lResult.Add((TBaseType) JsonSerializer.Deserialize(ref uReader, _tType, jOptions));
                        }
                    }
                    return (_lResult);
                }
            }
            public override void Write(Utf8JsonWriter uWriter, IEnumerable<TBaseType> iValue, JsonSerializerOptions jOptions)
            {
                if (uWriter == null) 
                    throw new ArgumentNullException(nameof(uWriter));
                else if (iValue == null)
                    throw new ArgumentNullException(nameof(iValue));
                else
                {
                    uWriter.WriteStartObject();
                    {
                        foreach (var _tVal in iValue)
                        {
                            uWriter.WritePropertyName(_tVal.GetType().FullName);
                            JsonSerializer.Serialize(uWriter, _tVal, _tVal.GetType(), jOptions);
                        }
                    }
                    uWriter.WriteEndObject();
                }
            }
            #endregion
        }
    }
    #endregion


    #region Extensions
    public static class ExtArray
    {
        public static int IndexOf<T>(this T[] tArray, T tValue)
        {
            return (Array.IndexOf(tArray, tValue));
        }
        public static T[] RemoveAt<T>(this T[] tArray, int iIndex)
        {
            UtlArray.RemoveAt(ref tArray, iIndex);
            return (tArray);
        }
    }
    public static class ExtString
    {
        public static string EnsureEndsWith(this string sValue, string sEnd)
        {
            Debug.Assert(sValue != null);

            if (sValue.EndsWith(sEnd))
                return (sValue);
            else
                return (sValue + sEnd);
        }
        public static bool TryRemoveStart(this string sValue, string sStart, out string sResult)
        {
            Debug.Assert(sValue != null);

            if (sValue.StartsWith(sStart))
            {
                sResult = sValue.Substring(sStart.Length, (sValue.Length - sStart.Length));
                return (true);
            }
            else
            {
                sResult = sValue;
                return (false);
            }
        }
        public static string RemoveStart(this string sValue, string sStart)
        {
            string sResult;
            TryRemoveStart(sValue, sStart, out sResult);
            return (sResult);
        }
        public static bool TryRemoveEnd(this string sValue, string sEnd, out string sResult)
        {
            Debug.Assert(sValue != null);

            if (sValue.EndsWith(sEnd))
            {
                sResult = sValue.Substring(0, (sValue.Length - sEnd.Length));
                return (true);
            }
            else
            {
                sResult = sValue;
                return (false);
            }
        }
        public static string RemoveEnd(this string sValue, string sEnd)
        {
            string sResult;
            TryRemoveEnd(sValue, sEnd, out sResult);
            return (sResult);
        }
        public static Stream ToStream(this string sValue)
        {
            return (new MemoryStream(Encoding.UTF8.GetBytes(sValue)));
        }
        public static string CreateMd5Hash(this string sValue)
        {
            byte[] _bBytes = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(sValue));
            return (BitConverter.ToString(_bBytes).Replace("-", string.Empty));
        }
        /// Finds an index-declaration (e.g. "string[index]").
        public static bool FindIndexDeclaration(this string sValue, out int iStart, out int iEnd)
        {
            iStart = sValue.IndexOf('[');
            if (iStart != -1)
            {
                iEnd = sValue.IndexOf(']', (iStart+1));
                if (iEnd != -1)
                    return (true);
            }

            iStart = -1;
            iEnd = -1;
            return (false);
        }
        /// Parses an index-declaration (e.g. "string[index]").
        public static int ParseIndex(this string sValue)
        {
            int iResult;
            if (TryParseIndex(sValue, out iResult))
                return (iResult);
            else
                throw new MissingFieldException("Index not defined in string!");
        }
        /// Parses an index-declaration (e.g. "string[index]").
        public static bool TryParseIndex(this string sValue, out int iIndex)
        {
            int _iStart, _iEnd;
            return (TryParseIndex(sValue, out _iStart, out _iEnd, out iIndex));
        }
        /// Parses an index-declaration (e.g. "string[index]").
        public static bool TryParseIndex(this string sValue, out int iStart, out int iEnd, out int iIndex)
        {
            if (FindIndexDeclaration(sValue, out iStart, out iEnd))
            {
                if ((iEnd-iStart) < 2)
                    ; // No index value defined.
                else if (int.TryParse(sValue.Substring((iStart+1), (iEnd-iStart-1)), out iIndex))
                    return (true);
            }
            
            iIndex = -1;
            return (false);
        }
        /// Parses an index-declaration (e.g. "string[index]") and removes it from string.
        public static string PopIndex(this string sValue, out int iIndex)
        {
            int _iStart, _iEnd;
            if (TryParseIndex(sValue, out _iStart, out _iEnd, out iIndex))
                return (sValue.Remove(_iStart, (_iEnd-_iStart+1)));
            else
                return (sValue);
        }
    }
    public static class ExtDictionary
    {
        /// <summary>
        /// Updates an existing value or else adds a new value.
        /// </summary>
        /// <param name="uValue">Value to apply to.</param>
        public static void AddOrUpdate<T, U>(this IDictionary<T, U> dSource, T tKey, U uValue) => dSource.AddOrUpdate(tKey, () => uValue, (oldVal) => uValue);
        /// <inheritdoc cref="AddOrUpdate" />
        /// <param name="updateValue">Delegate to update an existing value.</param>
        public static void AddOrUpdate<T, U>(this IDictionary<T, U> dSource, T tKey, Func<U, U> updateValue) => dSource.AddOrUpdate(tKey, () => default(U), updateValue);
        /// <inheritdoc cref="AddOrUpdate" />
        /// <param name="updateValue">Delegate to update an existing value.</param>
        public static void AddOrUpdate<T, U>(this IDictionary<T, U> dSource, T tKey, Func<U> newValue, Func<U, U> updateValue)
        {
            if (dSource.TryGetValue(tKey, out var tVal))
                dSource[tKey] = updateValue(tVal);
            else
                dSource.Add(tKey, newValue());
        }
        /// <summary>
        /// Gets the value of the specified key or creates (And add to the dictionary) a new value instance.
        /// </summary>
        /// <param name="dSource"></param>
        /// <param name="tKey"></param>
        /// <returns></returns>
        public static U GetOrCreateValue<T, U>(this IDictionary<T, U> dSource, T tKey)
        {
            U uResult;
            if (!dSource.TryGetValue(tKey, out uResult))
            {
                uResult = (U)Activator.CreateInstance(typeof(U));
                dSource.Add(tKey, uResult);
            }
            return (uResult);
        }
        public static void ForEach<T, U>(this IDictionary<T, U> d, Action<KeyValuePair<T, U>> a)
        {
            foreach (KeyValuePair<T, U> p in d)
                a(p);
        }
        public static void ForEach<T, U>(this Dictionary<T, U>.KeyCollection k, Action<T> a)
        {
            foreach (T t in k)
                a(t);
        }
        public static void ForEach<T, U>(this Dictionary<T, U>.ValueCollection v, Action<U> a)
        {
            foreach (U u in v)
                a(u);
        }
        public static U Pop<T, U>(this IDictionary<T, U> dSource, T tKey)
        {
            U uResult = dSource[tKey];
            dSource.Remove(tKey);
            return (uResult);
        }
        public static bool TryPop<T, U>(this IDictionary<T, U> dSource, T tKey, out U tValue)
        {
            if (dSource.TryGetValue(tKey, out tValue))
            {
                dSource.Remove(tKey);
                return (true);
            }
            else
                return (false);
        }
    }
    public static class ExtCollection
    {
        public static T FirstOrDefault<T>(this ICollection iCollection)
        {
            List<T> lList = iCollection.OfType<T>().ToList();
            return (lList.Any() ? lList.First() : default(T));
        }
    }
    public static class ExtEnumerable
    {
        public static int GetCount(this IEnumerable iSource)
        {
            if (iSource is Array _aArr)
                return (_aArr.Length);
            else if (iSource is IList _iList)
                return (_iList.Count);
            else
            {
                int _iCount = 0;
                foreach (var _iVar in iSource)
                    _iCount++;
                return (_iCount);
            }
        }
        public static object ElementAt(this IEnumerable iSource, int iIndex)
        {
            if (iSource is Array _aArr)
                return (_aArr.GetValue(iIndex));
            else if (iSource is IList _iList)
                return (_iList[iIndex]);
            else
            {
                int i = 0;
                foreach (var _iVar in iSource)
                {
                    if (i++ == iIndex)
                        return (_iVar);
                }
                throw new IndexOutOfRangeException();
            }
        }
        
        public static IEnumerable<T> First<T>(this IEnumerable<T> iSource, int iCount)
        {
            T[] tResult = new T[iCount];
            int _i = 0;
            foreach (T _tItem in iSource)
            {
                if (_i < iCount)
                    tResult[_i++] = _tItem;
                else
                    break;
            }
            return (tResult);
        }
        public static IEnumerable<T> Last<T>(this IEnumerable<T> iSource, int iCount)
        {
            int _iSrcCount = Enumerable.Count(iSource);
            iCount = System.Math.Min(iCount, _iSrcCount);

            int _i = 0;
            int _iStart = (_iSrcCount - iCount);
            T[] tResult = new T[iCount];
            foreach (T _tItem in iSource)
            {
                if (_i >= _iStart)
                    tResult[_i - _iStart] = _tItem;
                _i++;
            }
            return (tResult);
        }
        public static void ForEach<T>(this IEnumerable<T> iSource, Action<T> aAction)
        {
            foreach (T tElement in iSource)
                aAction(tElement);
        }
        public static bool IsUnique<T>(this IEnumerable<T> iSource)
        {
            return (iSource.All(new HashSet<T>().Add));  
        }
        public static bool IsEmpty<T>(this IEnumerable<T> iSource)
        {
            return ((iSource == null) || (!iSource.Any()));
        }
        public static int IndexOf<T>(this IEnumerable<T> iSource, T tValue)
        {
            int iIndex = 0;
            var vComparer = EqualityComparer<T>.Default; // or pass in as a parameter
            foreach (T item in iSource)
            {
                if (vComparer.Equals(item, tValue))
                    return (iIndex);
                else
                    iIndex++;
            }
            return (-1);
        }
        public static int IndexOf<T>(this IEnumerable<T> iSource, Predicate<T> pCondition)
        {
            int iIndex = 0;
            foreach (T item in iSource)
            {
                if (pCondition(item))
                    return (iIndex);
                else
                    iIndex++;
            }
            return (-1);
        }
        /// <summary>
        /// Returns a number of elements.
        /// </summary>
        public static IEnumerable<T> ElementsAt<T>(this IEnumerable<T> iSource, int iCount, int iOffset = 0)
        {
            foreach (var _iItem in iSource)
            {
                if (iOffset > 0)
                    iOffset--;
                else if (iCount == 0)
                    break;
                else
                {
                    yield return (_iItem);
                    iCount--;
                }
            }
        }
        /// <summary>
        /// Returns multiple elements at given indices.
        /// </summary>
        public static IEnumerable<T> ElementsAt<T>(this IEnumerable<T> iSource, IEnumerable<int> iIndices)
        {
            int i = 0;
            foreach (var _iItem in iSource)
            {
                if (iIndices.Contains(i++))
                    yield return (_iItem);
            }
        }
        /// <summary>
        /// Executs action for each object in separate task and waits for cempletion of every single task.
        /// </summary>
        public static void AsWaitingTasks<T>(this IEnumerable<T> iSource, Action aAction)
        {
            List<Task> lTasks = new List<Task>();
            iSource.ForEach(_iObject => {
                lTasks.Add(Task.Factory.StartNew(aAction));
            });
            Task.WaitAll(lTasks.ToArray());
        }
        /// <summary>
        /// Executs action for each object in separate task and waits for cempletion of every single task.
        /// </summary>
        public static void AsWaitingTasks<T>(this IEnumerable<T> iSource, Action<T> aAction)
        {
            List<Task> lTasks = new List<Task>();
            iSource.ForEach(_iObject => {
                lTasks.Add(Task.Factory.StartNew(
                    new Action<object>(_oObj => aAction((T)_oObj)),
                    _iObject));
            });
            Task.WaitAll(lTasks.ToArray());
        }
        public static IEnumerable<Tuple<TItem1>> ToTuple<T, TItem1>(this IEnumerable<T> iSource, Func<T, TItem1> fSelector)
            where T : notnull
        {
            foreach (var _tItem in iSource)
                yield return (new Tuple<TItem1>(fSelector(_tItem)));
        }
        public static IEnumerable<Tuple<TItem1, TItem2>> ToTuple<T, TItem1, TItem2>(this IEnumerable<T> iSource, Func<T, TItem1> fSelector1, Func<T, TItem2> fSelector2)
            where T : notnull
        {
            foreach (var _tItem in iSource)
                yield return (new Tuple<TItem1, TItem2>(fSelector1(_tItem), fSelector2(_tItem)));
        }
        public static IEnumerable<Tuple<TItem1, TItem2, TItem3>> ToTuple<T, TItem1, TItem2, TItem3>(this IEnumerable<T> iSource, Func<T, TItem1> fSelector1, Func<T, TItem2> fSelector2, Func<T, TItem3> fSelector3)
            where T : notnull
        {
            foreach (var _tItem in iSource)
                yield return (new Tuple<TItem1, TItem2, TItem3>(fSelector1(_tItem), fSelector2(_tItem), fSelector3(_tItem)));
        }
        public static IEnumerable<Tuple<TItem1, TItem2, TItem3, TItem4>> ToTuple<T, TItem1, TItem2, TItem3, TItem4>(this IEnumerable<T> iSource, Func<T, TItem1> fSelector1, Func<T, TItem2> fSelector2, Func<T, TItem3> fSelector3, Func<T, TItem4> fSelector4)
            where T : notnull
        {
            foreach (var _tItem in iSource)
                yield return (new Tuple<TItem1, TItem2, TItem3, TItem4>(fSelector1(_tItem), fSelector2(_tItem), fSelector3(_tItem), fSelector4(_tItem)));
        }

        public static IEnumerable<Tuple<TItem1, TItem2, TItem3, TItem4, TItem5>> ToTuple<T, TItem1, TItem2, TItem3, TItem4, TItem5>(this IEnumerable<T> iSource, Func<T, TItem1> fSelector1, Func<T, TItem2> fSelector2, Func<T, TItem3> fSelector3, Func<T, TItem4> fSelector4, Func<T, TItem5> fSelector5)
            where T : notnull
        {
            foreach (var _tItem in iSource)
                yield return (new Tuple<TItem1, TItem2, TItem3, TItem4, TItem5>(fSelector1(_tItem), fSelector2(_tItem), fSelector3(_tItem), fSelector4(_tItem), fSelector5(_tItem)));
        }
    }
    public static class ExtList
    {
        /// <summary>
        /// Adds an item only if its not existing in list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lSource"></param>
        /// <param name="tItem"></param>
        /// <returns></returns>
        public static bool AddIfNew<T>(this IList<T> lSource, T tItem)
        {
            if (!lSource.Contains(tItem))
            {
                lSource.Add(tItem);
                return (true);
            }
            else
                return (false);
        }
        public static void FromArray<T>(this IList<T> lSource, T[] tArray)
        {
            lSource.Clear();
            tArray.ForEach(_tItem => lSource.Add(_tItem));
        }
        public static void Move<T>(this IList<T> lSource, int iOldIndex, int iNewIndex)
        {
            if (iOldIndex != iNewIndex)
            {
                var _tItem = lSource[iOldIndex];
                lSource.RemoveAt(iOldIndex);
                if (iNewIndex > iOldIndex)
                    // Shift (re)moved index:
                    iNewIndex--;
                lSource.Insert(iNewIndex, _tItem);
            }
        }
        public static void Move<T>(this IList<T> lSource, T tItem, int iNewIndex)
        {
            Move(lSource, lSource.IndexOf(tItem), iNewIndex);
        }
        public static bool RemoveRange<T>(this IList<T> iSource, IEnumerable<T> iItems)
        {
            bool bResult = true;
            for (int i = (iItems.Count() - 1); i >= 0; i--)
            {
                if (!iSource.Remove(iItems.ElementAt<T>(i)))
                    bResult = false;
            }
            return (bResult);
        }
    }
    public static class ExtTimer
    {
        public static void Restart(this Timer tTimer)
        {
            tTimer.Stop();
            tTimer.Start();
        }
    }
    internal static class ExtRandom
    {
        public static T Next<T>(this Random source, T? minimum = null, T? maximum = null)
            where T : struct, Enum
        {
            var values = System.Enum.GetValues<T>();
            if ((minimum is not null) || (maximum is not null))
                // Limit value range:
                values = values
                    .Where(val => (minimum ?.CompareTo(val) ??0) <= 0)
                    .Where(val => (maximum ?.CompareTo(val) ??0) >= 0)
                    .ToArray();
            if (values.IsEmpty())
                throw new IndexOutOfRangeException($"Unable to get random value between '{minimum}' and '{maximum}'!");
            else
            {
                var index = source.Next(0, (values.Length-1));
                return (values[index]);
            }
        }
    }
    public static class ExtType
    {
        #region Enumerations
        public enum Purpose
        {
            eNull,

            eClass,
            eDictionary,
            eEnumerable,
            ePrimitive
        }
        #endregion


        public static Purpose GetPurpose(this Type tType)
        {
            switch (Type.GetTypeCode(tType))
            {
                case TypeCode.Empty:
                case TypeCode.DBNull:
                    return (Purpose.eNull);

                case TypeCode.Object:
                    if (typeof(IDictionary).IsAssignableFrom(tType))
                        return (Purpose.eDictionary);
                    else if (typeof(IEnumerable).IsAssignableFrom(tType))
                        return (Purpose.eEnumerable);
                    else
                        return (Purpose.eClass);

                default:
                    return (Purpose.ePrimitive);
            }
        }
    }
    public static class ExtComponentModel
    {
        public static bool IsBrowsable(this MemberInfo mInfo)
        {
            var iBrowsable = mInfo.GetCustomAttributes<BrowsableAttribute>();
            return ((iBrowsable.Count() == 0) || (iBrowsable.Contains(BrowsableAttribute.Yes)));
        }
        /// Filters properties with the desired attribute applied.
        public static Dictionary<PropertyInfo, T> FilterProperties<T>(this Type tType) where T : Attribute
        {
            var dResult = new Dictionary<PropertyInfo, T>();
            T _tAttrib;
            foreach (var _pProp in tType.GetProperties())
            {
                if (_pProp.TryGetCustomAttribute<T>(out _tAttrib))
                    dResult.Add(_pProp, _tAttrib);
            }
            return (dResult);
        }
        /// Filter categories of containing properties.
        public static Dictionary<string, List<PropertyInfo>> CategorizeProperties(this Type tType)
        {
            var dResult = new Dictionary<string, List<PropertyInfo>>();
            string _sCategory;
            foreach (var _pProp in tType.GetProperties())
            {
                if (_pProp.TryGetAttributeValue<CategoryAttribute, string>(out _sCategory))
                    dResult.GetOrCreateValue(_sCategory).Add(_pProp);
            }
            return (dResult);
        }
        public static T GetCustomAttribute<T>(this PropertyInfo pInfo) where T : Attribute
        {
            T _tAttrib;
            if (TryGetCustomAttribute<T>(pInfo, out _tAttrib))
                return (_tAttrib);
            else
                throw new FieldAccessException(string.Format("Failed to get custom attribute of type '{0}'!", typeof(T).Name));
        }
        public static bool TryGetCustomAttribute<T>(this MemberInfo mInfo, out T tResult) where T : Attribute
        {
            var _dAttr = mInfo.GetCustomAttributes<T>();
            if ((_dAttr != null) && (_dAttr.Count() >= 1))
                tResult = _dAttr.First<T>();
            else
                tResult = null;
            return (tResult != null);
        }
        /// <typeparam name="T">Attribute type</typeparam>
        /// <typeparam name="V">Attribute value type</typeparam>
        public static V GetAttributeValueOrDefault<T, V>(this MemberInfo mInfo) where T : Attribute
        {
            V vVal = default(V);
            mInfo.TryGetAttributeValue<T, V>(out vVal);
            return (vVal);
        }
        /// <typeparam name="T">Attribute type</typeparam>
        /// <typeparam name="V">Attribute value type</typeparam>
        /// <param name="fSelector">Selector to chose property to receive value from.</param>
        public static V GetAttributeValueOrDefault<T, V>(this MemberInfo mInfo, Func<T, V> fSelector) where T : Attribute
        {
            V vVal = default(V);
            mInfo.TryGetAttributeValue<T, V>(out vVal, fSelector);
            return (vVal);
        }
        /// <typeparam name="T">Attribute type</typeparam>
        /// <typeparam name="V">Attribute value type</typeparam>
        public static V GetAttributeValue<T, V>(this MemberInfo mInfo) where T : Attribute
        {
            V vResult;
            if (TryGetAttributeValue<T, V>(mInfo, out vResult))
                return (vResult);
            else
                throw new FieldAccessException(string.Format("Failed to get value of attribute '{0}'!", typeof(T).Name));
        }
        /// <typeparam name="T">Attribute type</typeparam>
        /// <typeparam name="V">Attribute value type</typeparam>
        /// <param name="fSelector">Selector to chose property to receive value from.</param>
        public static V GetAttributeValue<T, V>(this MemberInfo mInfo, Func<T, V> fSelector) where T : Attribute
        {
            V vResult;
            if (TryGetAttributeValue<T, V>(mInfo, out vResult, fSelector))
                return (vResult);
            else
                throw new FieldAccessException(string.Format("Failed to get value of attribute '{0}'!", typeof(T).Name));
        }
        /// <typeparam name="T">Attribute type</typeparam>
        /// <typeparam name="V">Attribute value type</typeparam>
        public static bool TryGetAttributeValue<T, V>(this MemberInfo mInfo, out V oValue) where T : Attribute
        {
            Debug.Assert((mInfo != null), "[GA1377]");

            bool bResult = false;
            object oResult = null;
            T _tAttribute;
            if (TryGetCustomAttribute<T>(mInfo, out _tAttribute))
            {
                switch (_tAttribute)
                {
                    case CategoryAttribute _cAttrib:
                        oResult = _cAttrib.Category;
                        break;
                    case DisplayNameAttribute _dAttrib:
                        oResult = _dAttrib.DisplayName;
                        break;
                    case DescriptionAttribute _dAttrib:
                        oResult = _dAttrib.Description;
                        break;
                    case ReadOnlyAttribute _rAttrib:
                        oResult = _rAttrib.IsReadOnly;
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Failed to get value of not implemented attribute {0}!", typeof(T).ToString()));
                }
                bResult = true;
            }
            if (oResult == null)
            {
                // Default value:
                if (typeof(T).IsAssignableFrom(typeof(DisplayNameAttribute)))
                    oResult = mInfo.Name;
            }
            
            if (oResult == null)
                oValue = default(V);
            else
                oValue = (V)oResult;

            return (bResult);
        }
        /// <typeparam name="T">Attribute type</typeparam>
        /// <typeparam name="V">Attribute value type</typeparam>
        /// <param name="fSelector">Selector to chose property to receive value from.</param>
        public static bool TryGetAttributeValue<T, V>(this MemberInfo mInfo, out V oValue, Func<T, V> fSelector) where T : Attribute
        {
            T _tAttribute;
            if (TryGetCustomAttribute<T>(mInfo, out _tAttribute))
            {
                oValue = fSelector(_tAttribute);
                return (true);
            }
            else
            {
                oValue = default(V);
                return (false);
            }
        }
        public static bool TryGetValue(this PropertyInfo pProperty, object oInstance, out object oResult, int iIndex = -1)
        {
            oResult = pProperty.GetValue(oInstance);
            if (oResult is IEnumerable _iEnm)
            {
                if (iIndex < 0)
                    throw new ArgumentException(string.Format("No index defined for 'indexable' type '{0}'!", pProperty.PropertyType.Name));
                else
                {
                    if (iIndex < _iEnm.GetCount())
                        oResult = _iEnm.ElementAt(iIndex);
                    else
                    {
                        oResult = null;
                        return (false);
                    }
                }
            }
            else if (iIndex != -1)
                throw new ArgumentException(string.Format("Index defined for 'non-indexable' type '{0}'!", pProperty.PropertyType.Name));

            return (true);
        }
        public static object GetValue(this PropertyInfo pProperty, object oInstance, int iIndex = -1)
        {
            var oResult = pProperty.GetValue(oInstance);
            if ((oResult is IEnumerable) || (iIndex != -1))
                oResult = ((IEnumerable)oResult).ElementAt(iIndex);
            return (oResult);
        }
        public static void SetValue(this PropertyInfo pProperty, object oInstance, object oValue, int iIndex = -1)
        {
            if (iIndex == -1)
                pProperty.SetValue(oInstance, oValue);
            else
            {
                if (pProperty.PropertyType.IsArray)
                    pProperty.SetValue(oInstance, oValue, new object[] { iIndex });
                else if (typeof(IList).IsAssignableFrom(pProperty.PropertyType))
                {
                    var _iList = (IList)pProperty.GetValue(oInstance);
                    if (iIndex < _iList.Count)
                        _iList[iIndex] = oValue;
                    else if (iIndex == _iList.Count)
                        _iList.Add(oValue);
                    else
                        throw new IndexOutOfRangeException();
                }
                else
                    throw new ArgumentException(string.Format("Unable to set value of type '{0}'!", pProperty.PropertyType.Name));
            }
        }
    }
    public static class ExtEnum
    {
        public static IEnumerable<TEnum> GetFlags<TEnum>(this TEnum tSource)
            where TEnum : Enum
        {
            foreach (Enum _eFlag in Enum.GetValues(tSource.GetType()))
            {
                if (tSource.HasFlag(_eFlag))
                    yield return ((TEnum)_eFlag);
            }
        }
        /// Tests if flags are specified and value is not <c>0</c>.
        public static bool TestFlag<TEnum>(this TEnum tSource, TEnum tFlags)
            where TEnum : Enum
        {
            if (tSource.Equals(default(TEnum)))
                return (false);
            else if (tFlags.Equals(default(TEnum)))
                return (false);
            else
                return (tSource.HasFlag(tFlags));
        }
    }
    public static class ExtStream
    {
        public static void CopyTo(this Stream sSource, Stream sTarget, int iCount, int iBufferSize = 1024)
        {
            byte[] _bBuf = new byte[iBufferSize];
            int _iRead;
            while (iCount > 0)
            {
                _iRead = sSource.Read(_bBuf, 0, (int)Math.Min(iCount, (long)_bBuf.Length));
                if (_iRead > 0)
                {
                    sTarget.Write(_bBuf, 0, _iRead);
                    iCount -= _iRead;
                }
            }
        }
    }
    public static class Extension_HttpClient
    {
        public static async Task DownloadFileAsync(this HttpClient source, string url, string filePath, FileMode mode = FileMode.Create) => await source.DownloadFileAsync(new Uri(url), filePath, mode);
        public static async Task DownloadFileAsync(this HttpClient source, Uri requestUri, string filePath, FileMode mode = FileMode.Create)
        {
            using (var stream = await source.GetStreamAsync(requestUri))
            using (var fileStream = new FileStream(filePath, mode))
            {
                await stream.CopyToAsync(fileStream);
            }
        }
    }
    #endregion
}
