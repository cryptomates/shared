using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Shared.Utils.Core;

namespace Shared.Utils.Asp
{
    #region Extensions
    public static class ExtTempDataDictionary
    {
        /// Puts an object into the TempData by first serializing it to JSON.
        public static void Put<T>(this ITempDataDictionary iTempData, string sKey, T value, JsonConverter lConverter) where T : class
        {
            var jOptions = new System.Text.Json.JsonSerializerOptions();
            jOptions.Converters.Add(lConverter);
            Put<T>(iTempData, sKey, value, jOptions);
        }
        /// Puts an object into the TempData by first serializing it to JSON.
        public static void Put<T>(this ITempDataDictionary iTempData, string sKey, T value, JsonSerializerOptions jOptions = null) where T : class
        {
            iTempData[sKey] = JsonSerializer.Serialize(value, jOptions);
        }

        /// Gets an object from the TempData by deserializing it from JSON.
        public static bool TryGet<T>(this ITempDataDictionary iTempData, string sKey, out T tResult, JsonSerializerOptions jOptions = null) where T : class, new()
        {
            object _oObj;
            if (iTempData.TryGetValue(sKey, out _oObj))
                tResult = JsonSerializer.Deserialize<T>((string)_oObj, jOptions);
            else
                tResult = null;
            return (tResult != null);
        }
        /// Gets an object from the TempData by deserializing it from JSON.
        public static bool TryGet<T>(this ITempDataDictionary iTempData, string sKey, JsonConverter lConverter, out T tResult) where T : class, new()
        {
            var jOptions = new System.Text.Json.JsonSerializerOptions();
            jOptions.Converters.Add(lConverter);
            return (TryGet(iTempData, sKey, out tResult, jOptions));
        }
        /// Gets an object from the TempData by deserializing it from JSON.
        public static T Get<T>(this ITempDataDictionary iTempData, string sKey, JsonSerializerOptions jOptions = null) where T : class, new()
        {
            T tResult = null;
            if (TryGet(iTempData, sKey, out tResult, jOptions))
                return (tResult);
            else
                return (new T());
        }
        /// Gets an object from the TempData by deserializing it from JSON.
        public static T Get<T>(this ITempDataDictionary iTempData, string sKey, JsonConverter lConverter) where T : class, new()
        {
            var jOptions = new System.Text.Json.JsonSerializerOptions();
            jOptions.Converters.Add(lConverter);
            return (Get<T>(iTempData, sKey, jOptions));
        }
        /// Gets an object from the TempData by deserializing it from JSON.
        public static T Get<T>(this ITempDataDictionary iTempData, string sKey, T tDefaultValue) where T : new()
        {
            object _oObj;
            if (iTempData.TryGetValue(sKey, out _oObj))
            {
                if (_oObj is string)
                    return (JsonSerializer.Deserialize<T>((string)_oObj));
                else
                    return ((T)_oObj);
            }
            else
                return (tDefaultValue);
        }
    }
    public static class ExtSession
    {
        /// Sets an object into the session by first serializing it to JSON.
        public static void Set<T>(this ISession iSession, string sKey, T value, params JsonConverter[] lConverter) where T : class
        {
            var jOptions = new System.Text.Json.JsonSerializerOptions();
            lConverter.ForEach(_iCnv => jOptions.Converters.Add(_iCnv));
            Set<T>(iSession, sKey, value, jOptions);
        }
        /// Sets an object into the session by first serializing it to JSON.
        public static void Set<T>(this ISession iSession, string sKey, T value, JsonSerializerOptions jOptions = null) where T : class
        {
            iSession.SetString(sKey, JsonSerializer.Serialize(value, jOptions));
        }

        /// Gets an object from the session by deserializing it from JSON.
        public static bool TryGet<T>(this ISession iSession, string sKey, out T tResult, JsonSerializerOptions jOptions = null) where T : class, new()
        {
            var _sVal = iSession.GetString(sKey);
            if (_sVal != null)
                tResult = JsonSerializer.Deserialize<T>(_sVal, jOptions);
            else
                tResult = null;
            return (tResult != null);
        }
        /// Gets an object from the session by deserializing it from JSON.
        public static bool TryGet<T>(this ISession iSession, string sKey, JsonConverter lConverter, out T tResult) where T : class, new()
        {
            var jOptions = new System.Text.Json.JsonSerializerOptions();
            jOptions.Converters.Add(lConverter);
            return (TryGet(iSession, sKey, out tResult, jOptions));
        }
        /// Gets an object from the session by deserializing it from JSON.
        public static T Get<T>(this ISession iSession, string sKey, JsonSerializerOptions jOptions = null) where T : class, new()
        {
            T tResult = null;
            if (TryGet(iSession, sKey, out tResult, jOptions))
                return (tResult);
            else
                return (new T());
        }
        /// Gets an object from the session by deserializing it from JSON.
        public static T Get<T>(this ISession iSession, string sKey, params JsonConverter[] lConverter) where T : class, new()
        {
            var jOptions = new System.Text.Json.JsonSerializerOptions();
            lConverter.ForEach(_iCnv => jOptions.Converters.Add(_iCnv));
            return (Get<T>(iSession, sKey, jOptions));
        }
        /// Gets an object from the session by deserializing it from JSON.
        public static T Get<T>(this ISession iSession, string sKey, T tDefaultValue) where T : new()
        {
            if (iSession.TryGetValue(sKey, out var _sValue))
                return (JsonSerializer.Deserialize<T>(_sValue));
            else
                return (tDefaultValue);
        }
    }
    public static class ExtHtmlHelper
    {
        public static IHtmlContent Input(this IHtmlHelper iHtml, string sName, object oValue, bool bHidden = false)
        {
            string _sVal = oValue?.ToString() ??string.Empty;
            string _sType = (bHidden ? "hidden" : "text");
            return (new HtmlString(string.Format(@"<input type=""{0}"" name=""{1}"" value=""{2}""/>", _sType, sName, _sVal)));
        }
    }
    #endregion
}