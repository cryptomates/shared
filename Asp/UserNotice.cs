
using System;
using System.Collections.Generic;

namespace Shared.Asp.UserNotice
{
    #region Enumerations
    public enum NoticeType
    {
        eNotification,
        eInfo,
        eSuccess,
        eWarning,
        eError
    }
    #endregion
    #region Types
    public class UserNotice
    {
        public UserNotice(string sText, string sTitle = "", string sSubTitle = "", NoticeType aNoticeType = NoticeType.eInfo)
        {
            this.NoticeType = aNoticeType;
            this.Title = sTitle;
            this.SubTitle = sSubTitle;
            this.Text = sText;
        }
        public UserNotice(Exception eException, string sTitle = "", string sSubTitle = "")
        {
            this.NoticeType = NoticeType.eError;
            this.Title = sTitle;
            this.SubTitle = sSubTitle;
            this.Text = eException.Message;
        }


        #region Properties
        public NoticeType NoticeType { init; get; }
        public string Title { init; get; }
        public string SubTitle { init; get; }
        public string Text { init; get; }
        #endregion
    }
    public class UserNoticeQueue : Queue<UserNotice>
    {
    }
    #endregion
}