using System;
using System.IO;

namespace Shared.IO
{
    /// <summary>
    /// Stream that is limiting the length of a base stream.
    /// </summary>
    class LimitedStream : Stream
    {
        /// <summary>
        /// Executes a web request.
        /// </summary>
        /// <param name="sStream">Base stream.</param>
        /// <param name="lMaxLength">Max. length of the stream (relative to base stream's current position).</param>
        public LimitedStream(Stream sStream, long lMaxLength = 0)
        {
            long _lRemaining = (sStream.Length - sStream.Position);
            if (lMaxLength == 0)
                lMaxLength = _lRemaining;
            else
                lMaxLength = Math.Min(_lRemaining, lMaxLength);

            this.BaseStream = sStream;
            this.lLength = (sStream.Position + lMaxLength);
        }


        #region Properties.Management
        public Stream BaseStream { private set; get; }
        #endregion
        #region Properties
        public bool EndOfStream { get { return (Position >= (Length - 1)); } }

        public override bool CanRead => BaseStream.CanRead;
        public override bool CanSeek => BaseStream.CanSeek;
        public override bool CanWrite => BaseStream.CanWrite;
        public override long Length { get { return (lLength); } }
        private long lLength;
        public override long Position { get => BaseStream.Position; set => BaseStream.Position = value; }
        #endregion


        #region Management
        public override void Flush()
        {
            BaseStream.Flush();
        }
        public override int Read(byte[] bBuffer, int iOffset, int iCount)
        {
            if (EndOfStream)
                return (0);
            else
            {
                int iRead = BaseStream.Read(bBuffer, iOffset, Math.Min((int)(Length - Position - 1), iCount));
                if (EndOfStream)
                    // [Unknown] It seems as if the read operation has to be finalized by reading one last byte.
                    BaseStream.ReadByte();
                return (iRead);
            }
        }
        public override long Seek(long iOffset, SeekOrigin sOrigin)
        {
            return (BaseStream.Seek(iOffset, sOrigin));
        }
        public override void SetLength(long lValue)
        {
            BaseStream.SetLength(lValue);
        }
        public override void Write(byte[] bBuffer, int iOffset, int iCount)
        {
            BaseStream.Write(bBuffer, iOffset, iCount);
        }
        #endregion
    }
}