// [Generic navigation]
// - Description:
//   Provides *.cshtml based generic navigation for C# types.
// - Usage:
//   - 1) MyAction.cs:
//     public class MyModel { }                                                     // Custom model.
//     public class MyAction1 : NavAction { }                                       // Custom action(s).
//     public class AmModuleNavActionBinder : NavActionBinder<AmModuleSettings>     // Customize Action-Binder.
//     {
//         public AmModuleNavActionBinder()
//         {
//             SupportAction(typeof(MyAction1));                                    // Register customs actions.
//         }
//     }
//   - 2) MyView.cshtml:
//     MyModel mModel = new MyModel();                                              // Instance of custom model.
//     string sPostAction = "/MyForm/MyPostAction";                                 // Path to controller / post-action.
//     NavSettings<MyModel> nNav = new NavSettings<MyModel>(mModel, sPostAction);   // Instance of navigation settings.
//     <partial name="~/Shared/Asp/GenNav.cshtml" model="nNav"/>                    // Begin navigation by calling partial navigation-view.
//   - 3) MyController.cs:
//     public IActionResult MyGetAction([FromQuery] string property)
//     {
//          // ...                                                                  // Handling of get-actions.
//     }
//     [HttpPost]
//     public IActionResult MyPostAction([ModelBinder(typeof(AmModuleNavActionBinder))] NavAction aAction)
//     {
//          // ...                                                                  // Handling of (standard AND customized) post-actions.
//     }
// - Hints:
//   - Properties in general:
//     - Properties with a 'LayoutAttribute' will be forced be displayed by an appropriate layout.
//       More layout-attributes (like e.g. "ButtonLayoutAttribute" or the customizing "RenderedLayoutAttribute") are provided.
//     - Possible input-values of properties with a 'LimitedInputAttribute' will be filtered.
//       So certain values can be denied/allowed for the user to use.
//   - "Class" properties:
//     - Only sub-properties with a 'CategoryAttribute' are displayed.
//     - Sub-properties with a 'DisplayTextAttribute' are displayed as a (non editable) text.
//   - Interfaces:
//     - 'IContentProvider':
//       Enables inheriting classes to provide an enumeration of custom values to be displayed.

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Shared.Utils.Core;
using static Shared.Utils.Core.UtlComponentModel;


namespace Shared.Asp.GenNav
{
    #region Enumerations.Customization
    public enum TextStyle
    {
        [Description("alert-primary")]
        ePrimary,
        [Description("alert-secondary")]
        eSecondary,
        [Description("alert-success")]
        eSuccess,
        [Description("alert-danger")]
        eDanger,
        [Description("alert-warning")]
        eWarning,
        [Description("alert-info")]
        eInfo,
        [Description("alert-light")]
        eLight,
        [Description("alert-dark")]
        eDark,

        eNormal,
        eMuted,
    }
    public enum LayoutType
    {
        /// Primitive value displayed in one row.
        ePrimitive,
        /// Primitive value displayed in a text area.
        eAreaValue,
        /// Group of input values in one row.
        /// - DisplayName-Attribute serves as Input-Title (Optional).
        /// - Description-Attribute serves as Placeholder (Optional).
        eInputGroup,
        /// Displays a button.
        eButton,

        /// Value rendered by the attribute.
        eRendered
    }
    #endregion
    #region Attributes.Customization
    public abstract class FormatAttribute : Attribute
    {
        public FormatAttribute(string sFormat)
        {
            this.Format = sFormat;
        }


        #region Properties.Management
        public string Format { init; get; }
        #endregion


        #region Management
        public string FormatDefault(params object[] oArgs)
        {
            return (string.Format(Format, oArgs));
        }
        #endregion
    }
    public class CollectionItemDisplayNameAttribute : FormatAttribute
    {
        public CollectionItemDisplayNameAttribute(string sFormat) : base(sFormat)
        {
        }


        #region Management
        public string FormatDisplayName(int iIndex) => FormatDefault(iIndex+1);
        #endregion
    }
    /// Forces a properties layout.
    public class LayoutAttribute : Attribute
    {
        protected LayoutAttribute(byte bDummy, LayoutType lType)
        {
            this.Layout = lType;
        }
        public LayoutAttribute(LayoutType lType) : this(0, lType)
        {
            switch (lType)
            {
                case LayoutType.eButton:
                    throw new InvalidOperationException(string.Format("Please use the {0} to specify a button-layout!", nameof(ButtonLayoutAttribute))); 
            }
        }


        #region Properties.Management
        public LayoutType Layout { init; get; }
        #endregion
    }
    /// Forces a properties button-layout.
    public class ButtonLayoutAttribute : LayoutAttribute
    {
        public ButtonLayoutAttribute(string sIconClass) : base(0, LayoutType.eButton)
        {
            this.IconClass = sIconClass;
        }


        #region Properties.Management
        public string IconClass { init; get; }
        #endregion
    }
    /// Turns a variable into a notification that will be displayed during navigation.
    public class DisplayTextAttribute : Attribute
    {
        public DisplayTextAttribute(TextStyle nType)
        {
            this.Type = nType;
        }


        #region Properties.Management
        public TextStyle Type { init; get; }
        #endregion
    }
    [AttributeUsage((AttributeTargets.Property | AttributeTargets.Class), AllowMultiple = true)]
    public abstract class LimitedInputAttribute : Attribute
    {
        public LimitedInputAttribute(bool bPolarity, string sProperty, params object[] oValues)
        {
            this.Polarity = bPolarity;
            this.Property = sProperty;
            this.Values = oValues;
        }


        #region Properties.Management
        protected bool Polarity { init; get; }
        public string Property { internal set; get; }

        #endregion
        #region Properties
        public object[] Values { init; get; }
        #endregion
    }
    public class AllowedInputAttribute : LimitedInputAttribute
    {
        public AllowedInputAttribute(string sProperty, params object[] eValues) : base(true, sProperty, eValues)
        {
        }
        public AllowedInputAttribute(params object[] eValues) : base(true, "", eValues)
        {
        }
    }
    public class DeniedInputAttribute : LimitedInputAttribute
    {
        public DeniedInputAttribute(string sProperty, params object[] eValues) : base(false, sProperty, eValues)
        {
        }
        public DeniedInputAttribute(params object[] eValues) : base(false, "", eValues)
        {
        }
    }
    /// Overrides the rendering of a layout.
    ///<example>
    ///public class TestMiiAttribute : RenderedLayoutAttribute
    ///{
    ///     public override IHtmlContent Render() => new HtmlString("<b>some value</b>")
    ///}
    ///</example>
    public abstract class RenderedLayoutAttribute : LayoutAttribute
    {
        public RenderedLayoutAttribute() : base(0, LayoutType.eRendered)
        {
        }

        
        #region Management
        public abstract IHtmlContent Render();
        #endregion
    }
    #endregion
    #region Interfaces.Customization
    /// Provides additional (user defined) content for navigation.
    public interface IContentProvider
    {
        #region Properties.Management
        IEnumerable<object> Content { get; }
        Type ContentType { get; }
        #endregion
    }  
    #endregion
    #region Types.Settings
    public class NavSettings
    {
        public NavSettings(object oModel, string sPostAction, string sGetAction = "")
        {
            // Apply settings:
            this.oModel = oModel;
            this.GetAction = sGetAction;
            this.PostAction = sPostAction;

            // Validate settings:
            if (!IsValid)
                throw new ArgumentException("Failed to validate navigation settings!");
        }


        #region Properties.Management
        public bool IsValid => ((oModel != null) && (!string.IsNullOrEmpty(PostAction)));
        public string GetAction { init; get; }
        public string PostAction { init; get; }
        #endregion


        #region Management
        public object GetModel() => oModel;
        #endregion


        private object oModel;
    }
    public class NavSettings<TModel> : NavSettings
    {
        public NavSettings(TModel tModel, string sPostAction, string sGetAction = "") : base(tModel, sPostAction, sGetAction)
        {
        }


        #region Properties.Management
        public TModel Model => (TModel)GetModel();
        #endregion
    }
    #endregion
    #region Types.Actions
    public class NavActionSettings
    {
        #region Properties
        /// <summary>
        /// Checkbox and radio values are only preserved when checked (Standard browser behaviour) :(
        /// Hence that, all boolean values can be reset before new values are applied by an action.
        /// </summary>
        public bool OptimizeBoolValues { set; get; } = false;
        #endregion
    }
    public abstract class NavAction
    {
        #region Constants
        public const string TAG_INTERNAL = "$";
        #endregion


        protected NavAction(NavActionBinder nBinder, string sKey)
        {
            this.Binder = nBinder;
            this.Key = sKey;
        }

        
        #region Properties.Management
        public NavActionBinder Binder { init; get; }
        public Type ModelType => Binder.ModelType;

        public string Key { init; get; }
        #endregion
        #region Properties
        public NavActionSettings Settings => Binder.Settings;
        #endregion


        #region Initialization
        /// Creates a new instance of the referred model-type and initializes it with the actions data.
        public object CreateModel()
        {
            if (ModelType == null)
                throw new ArgumentNullException("No model defined!");
            else
            {
                // Create model instance:
                var oModel = Activator.CreateInstance(ModelType);

                // Update model:
                UpdateModel(oModel);

                return (oModel);
            }
        }
        /// Updates a given model instance with the actions data.
        public void UpdateModel(object oModel)
        {
            if (oModel == null)
                throw new ArgumentNullException();
            var _tType = oModel.GetType();
            if ((ModelType != null) && (ModelType != _tType))
                throw new TypeLoadException(string.Format("Failed to update model because it is incompatible to the action's type '{0}'!", ModelType.FullName));
            else
            {
                var lToReset = new List<string>();
                if (Settings.OptimizeBoolValues)
                {
                    // Determine boolean values:
                    UtlComponentModel.EvaluateObject(oModel, (PropertyInfo pInfo, String sName, String sValue, int iDepth, ref Object oTag) => {
                        if (pInfo == null)
                            return (EvaluationResult.eSkip);
                        else
                        {
                            // Update path:
                            if (oTag is string _sPath)
                                oTag = string.Join('.', _sPath, pInfo.Name);
                            else
                                oTag = pInfo.Name;

                            if (pInfo.PropertyType == typeof(bool))
                                lToReset.Add(oTag as string);

                            return (EvaluationResult.eSuccess);
                        }
                    });
                }

                // Apply received values:
                var _lExcpt = new List<Exception>();
                var _pProperties = _tType.GetProperties();
                foreach (var _dEntry in Binder.Data)
                {
                    try
                    {
                        UtlComponentModel.SetValue(NavActionBinder.PropertyResolver, oModel, _dEntry.Key, (_tType) => {
                            if (_tType.IsEnum)
                                return (Enum.Parse(_tType, _dEntry.Value));
                            else if (_tType.Equals(typeof(bool)))
                            {
                                lToReset.Remove(_dEntry.Key);
                                return (UtlBoolean.Parse(_dEntry.Value));
                            }
                            else if (_tType.Equals(typeof(DateTime)))
                                return (DateTime.Parse(_dEntry.Value));
                            else if (_tType.Equals(typeof(TimeSpan)))
                                return (UtlTimeSpan.Parse(_dEntry.Value));
                            else
                                return (Convert.ChangeType(_dEntry.Value, _tType));
                        });
                    }
                    catch(Exception eExcpt)
                    {
                        eExcpt.Data.Add(_dEntry.Key, _dEntry.Value);
                        _lExcpt.Add(eExcpt);
                    }
                }
                if (!_lExcpt.IsEmpty())
                    throw new AggregateException(string.Format("Failed to parse {0} value(s) during model update!", _lExcpt.Count), _lExcpt);

                // Reset remaining boolean values:
                lToReset.ForEach(_sPath => UtlComponentModel.SetValue(NavActionBinder.PropertyResolver, oModel, _sPath, false));
            }
        }
        #endregion
        #region Format
        public static string Format(string sKey)
        {
            return (string.Format("{0}{1}", TAG_INTERNAL, sKey));
        }
        public static string Format(string sKey, object oValue)
        {
            return (string.Format("{0}{1}{0}{2}", TAG_INTERNAL, sKey, oValue));
        }
        public static string Format(string sKey, object oValue, int iIndex)
        {
            return (string.Format("{0}{1}{0}{2}[{3}]", TAG_INTERNAL, sKey, oValue, iIndex));
        }

        public static string FormatType() => Format("type");
        #endregion
        #region Evaluation
        public virtual void DoDefaultPost(object oModel)
        {
            throw new NotImplementedException(string.Format("Handling action of type '{0}' failed: No default-handler implemented!", this.GetType().Name));
        }
        #endregion
    }
    public abstract class PropertyNavAction : NavAction
    {
        protected PropertyNavAction(NavActionBinder nBinder, string sKey, string sPropertyName) : base(nBinder, sKey)
        {
            this.PropertyName = sPropertyName;
        }


        #region Properties.Management
        public string PropertyName { init; get; }
        #endregion


        #region Helper
        protected IList GetList(object oObject, string sPropertyName, out Type tItemType)
        {
            var _tSettingsType = oObject.GetType();
            var _pProperty = _tSettingsType.GetProperty(sPropertyName);
            if (_pProperty.PropertyType.GenericTypeArguments.Count() == 1)
            {
                tItemType = _pProperty.PropertyType.GenericTypeArguments.First();
                return ((IList)_pProperty.GetValue(oObject));
            }
            else
                throw new UnsupportedContentTypeException(string.Format("Unsupported generic type '{0}'!", _pProperty.PropertyType.Name));
        }
        #endregion
    }
    public class AddListItemNavAction : PropertyNavAction
    {
        #region Constants
        public const string KEY = "addListItem";
        #endregion


        public AddListItemNavAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY, sValue)
        {
        }


        #region Format
        public static string FormatName(string sPropertyName) => Format(KEY, sPropertyName);
        #endregion
        #region Evaluation
        public override void DoDefaultPost(object oModel)
        {
            // Add item to list-property:
            GetList(oModel, PropertyName, out var _tType).Add(Activator.CreateInstance(_tType));
        }
        #endregion
    }
    public class DeleteListItemNavAction : PropertyNavAction
    {
        #region Constants
        public const string KEY = "delListItem";
        #endregion


        public DeleteListItemNavAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY, "")
        {
            int _iIdx;

            this.PropertyName = sValue.PopIndex(out _iIdx);
            this.Index = _iIdx;
        }

        
        #region Properties.Management
        public int Index { init; get; }
        #endregion


        #region Format
        public static string FormatName(string sPropertyName) => Format(KEY, sPropertyName);
        #endregion
        #region Evaluation
        public override void DoDefaultPost(object oModel)
        {
            // Delete item from list-property:
            GetList(oModel, PropertyName, out var _tType).RemoveAt(Index);
        }
        #endregion
    }
    public class ButtonLayoutNavAction : PropertyNavAction
    {
        #region Constants
        public const string KEY = "button";
        #endregion


        public ButtonLayoutNavAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY, sValue)
        {
        }


        #region Format
        public static string FormatName(string sPropertyName) => Format(KEY, sPropertyName);
        #endregion
    }
    public class ApplyValuesAction : NavAction
    {
        #region Constants
        public const string KEY = "applyVal";
        #endregion


        public ApplyValuesAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
        }


        #region Format
        public static string FormatName() => Format(KEY);
        #endregion
    }
    /// Posts a value via script.
    public class PostValueNavAction : PropertyNavAction
    {
        #region Constants
        public const string KEY = "postValue";
        #endregion


        public PostValueNavAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY, sValue)
        {
        }


        #region Format
        public static string FormatName(string sPropertyName) => Format(KEY, sPropertyName);
        #endregion
        #region Evaluation
        public override void DoDefaultPost(object oModel)
        {
            // Nothing to do (Posted value already applied via "UpdateModel()" or "CreateModel()").
        }
        #endregion
    }
    /// Receives a value via script.
    public class GetValueNavAction : PropertyNavAction
    {
        #region Constants
        public const string KEY = "getValue";
        #endregion


        public GetValueNavAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY, sValue)
        {
        }


        #region Format
        public static string FormatName(string sPropertyName) => Format(KEY, sPropertyName);
        #endregion
    }
    #endregion
    #region Types.Binder
    public abstract class NavActionBinder
    {
        #region Types
        private class NavActionPropertyResolver : IPropertyResolver
        {
            #region Properties.Management
            public bool IsValid => ((PropertyType != null) && (Value != null));

            public Type PropertyType { private set; get; }
            public object Value { private set; get; }
            #endregion


            #region Initialization
            public bool ResolveProperty(object oInstance, string sName)
            {
                Value = null;
                PropertyType = null;

                switch (sName)
                {
                    case ContentProviderInfo.KEY:
                        if (oInstance is IContentProvider _iContent)
                        {
                            Value = _iContent.Content;
                            PropertyType = _iContent.ContentType;
                        }
                        else
                            throw new TypeLoadException(string.Format("Failed to resolve property due to expected, but missing implementation of interface '{0}'!", nameof(IContentProvider)));
                        break;

                    default:
                        return (false);
                }
                
                Debug.Assert(Value != null);
                return (true);
            }
            #endregion
            #region Management.Property
            public object ReadProperty() => Value;
            public void WriteProperty(object oValue)
            {
                // (BETA) ...
                throw new NotImplementedException("Writing value not implemented yet!");
            }
            #endregion
            #region Management.Item
            public bool TryReadItem(int iIndex, out object oResult)
            {
                oResult = null;
                switch (Value)
                {
                    case IEnumerable<object> _iEnum:
                        oResult = _iEnum.ElementAt(iIndex);
                        break;
                }
                return (oResult != null);
            }
            public void WriteItem(int iIndex, object oValue)
            {
                // (BETA) ...
                throw new NotImplementedException("Writing value not implemented yet!");
            }
            #endregion
        }
        #endregion


        #region Properties.Management
        public Type ModelType { protected set; get; }
        /// Properties, whose values have been applied.
        public IReadOnlyDictionary<string, string> Data => dData;
        protected Dictionary<string, string> dData = new Dictionary<string, string>();
        
        public static IPropertyResolver PropertyResolver = new NavActionPropertyResolver();
        #endregion
        #region Properties
        public NavActionSettings Settings { get; } = new NavActionSettings();
        #endregion
    }
    public abstract class NavActionBinder<TModel> : NavActionBinder, IModelBinder where TModel : class
    {
        public NavActionBinder()
        {
            SupportAction(typeof(AddListItemNavAction));
            SupportAction(typeof(DeleteListItemNavAction));
            SupportAction(typeof(ButtonLayoutNavAction));
            SupportAction(typeof(ApplyValuesAction));
            SupportAction(typeof(PostValueNavAction));
            SupportAction(typeof(GetValueNavAction));
        }


        #region Initialization
        protected void SupportAction(Type tType)
        {
            lSupportedActions.Add(tType);
        }
        #endregion
        #region Management
        public Task BindModelAsync(ModelBindingContext mContext)
        {
            if (mContext == null)
                throw new ArgumentNullException(nameof(mContext));
            else
            {
                // Filter model's keys and values:
                mContext.HttpContext.Request.Form.Keys
                    .Where(_sKey => !_sKey.StartsWith("__"))
                    .ForEach(_sKey =>
                    {
                        // Determine how values are to resolve:
                        var _sValProvider = mContext.ValueProvider.GetValue(_sKey);
                        switch (_sValProvider.Count())
                        {
                            case 1: // Default: Add key and (single) value.
                                dData.Add(_sKey, _sValProvider.FirstValue);
                                break;

                            case 2: // Dictionary: Ignore (value provider's) key and add the two (dictionary-) values as key (first) and value (seccond).
                                dData.Add(_sValProvider.ElementAt(0), _sValProvider.ElementAt(1));
                                break;
                            
                            default:
                                throw new NotImplementedException("Failed to interpret contents in unexpected format!");
                        }
                    });

                if (!dData.IsEmpty())
                {
                    // Determine model type:
                    if (dData.TryPop(NavAction.FormatType(), out var _sModelType))
                        ModelType = Assembly.GetExecutingAssembly().GetType(_sModelType);
                    else
                        ; // No model defined.

                    // Determine action type:
                    NavAction aAction = null;
                    foreach (var _tActionType in lSupportedActions)
                    {
                        var _sKey = (string)_tActionType.GetField("KEY").GetValue(_tActionType);
                        if (PopKey(ref dData, _sKey, out var _sVal))
                        {
                            aAction = (NavAction)Activator.CreateInstance(_tActionType, new object[] { this, _sVal });
                            break;
                        }
                    }
                    if (aAction == null)
                        throw new NotImplementedException("Not implemented action'!");

                    mContext.Result = ModelBindingResult.Success(aAction);
                }
            }

            return (Task.CompletedTask);
        }
        #endregion


        #region Helper
        private bool PopKey(ref Dictionary<string, string> dData, string sKey, out string sValue)
        {
            sValue = "";
            var _sResult = dData.Keys.FirstOrDefault(_sKey =>
            {
                var _sVal = _sKey.Split(NavAction.TAG_INTERNAL, StringSplitOptions.RemoveEmptyEntries);
                return ((!_sVal.IsEmpty()) && (_sVal.First().Equals(sKey)));
            });
            if (string.IsNullOrEmpty(_sResult))
                ;
            else if (!_sResult.TryRemoveStart(NavAction.Format(sKey), out sValue))
                ;
            else
            {
                // Remove tag (if value provided):
                sValue = sValue.RemoveStart(NavAction.TAG_INTERNAL);

                dData.Remove(_sResult);
                return (true);
            }
            return (false);
        }
        #endregion

        private List<Type> lSupportedActions = new List<Type>();
    }
    #endregion
    #region Types.Info
    public class ObjectInfo
    {
        private ObjectInfo(ObjectInfo oOwner, bool bInherited)
        {
            this.Embed = (oOwner == null);
        }
        /// Constructor for inherited types.
        protected ObjectInfo(ObjectInfo oOwner) : this(oOwner, true)
        {
            this.Owner = oOwner;
            this.Parent = Owner?.Parent;
            
            CopyFrom(oOwner);

            // Determine limitations:
            GrabLimitations();
            GetLimitations(Type);
        }
        public ObjectInfo(PropertyInfo pProperty, ObjectInfo oParent = null) : this(oParent, false)
        {
            this.Property = pProperty;
            this.Parent = oParent;
            
            if (oParent != null)
            {
                this.Id = (oParent.Id + '.');
                this.Name = (oParent.Name + '.');
            }

            this.ReadOnly |= !pProperty.CanRead;
            this.ReadOnly |= pProperty.GetAttributeValueOrDefault<ReadOnlyAttribute, bool>();
            this.Id += pProperty.Name.ToLower();
            this.Name += pProperty.Name;
            this.Title = pProperty.GetAttributeValueOrDefault<DisplayNameAttribute, string>();
            this.Description = pProperty.GetAttributeValueOrDefault<DescriptionAttribute, string>();

            // Determine limitations:
            GrabLimitations();
            GetLimitations(Type);
            GetLimitations(Property);
        }


        #region Properties.Management
        public ObjectInfo Parent { init; get; }
        public ObjectInfo Owner { init; get; }
        public virtual PropertyInfo Property { private set; get; }
        public virtual Type Type => Property?.PropertyType;
        public bool ReadOnly { private set; get; }
        /// Embeds instance into a separate cell (row and column).
        public bool Embed { set; get; }
        /// Object is rendered by a control (e.g. Textbox)
        public virtual bool RenderControl { get { return (true); } }
        public IReadOnlyList<LimitedInputAttribute> Limits => lLimits;
        private List<LimitedInputAttribute> lLimits = new List<LimitedInputAttribute>();
        #endregion
        #region Properties
        public string Id { protected set; get; }
        public string Name { protected set; get; }
        /// Rendered name in HTML views.
        public string RenderName => (ReadOnly ? "" : Name);
        /// Instance based name (Without concatenated parts from parents)
        public string InstanceName => Property?.Name;
        public string Title { protected set; get; }
        public string Description { protected set; get; }
        #endregion


        #region Initialization
        protected virtual void CopyFrom(ObjectInfo oSource)
        {
            //this.Parent = oSource.Parent;
            //this.Owner = oSource.Owner;
            this.Property = oSource.Property;
            //this.Embed = oSource.Embed;
            //this.lLimits = oSource.lLimits;
            this.ReadOnly = oSource.ReadOnly;
            this.Id = oSource.Id;
            this.Name = oSource.Name;
            this.Title = oSource.Title;
            this.Description = oSource.Description;
        }
        // Grab limitations from parent.
        private int GrabLimitations()
        {
            if (Parent != null)
            {
                // Find matching limitations:
                var _lGrabbed = Parent.lLimits.Where(_lLimit => (_lLimit.Property == InstanceName)).ToList();
                if (!_lGrabbed.IsEmpty())
                {
                    // Remove from parent:
                    Parent.lLimits.RemoveRange(_lGrabbed);

                    // Add grabbed attributes:
                    _lGrabbed.ForEach(_lLimit => { _lLimit.Property = ""; });
                    this.lLimits.AddRange(_lGrabbed);

                    return (_lGrabbed.Count());
                }
            }
            return (0);
        }
        private void GetLimitations(Type tType)
        {
            if (tType != null)
            {
                if (tType.IsGenericType)
                    tType.GenericTypeArguments.ForEach(_GenType => GetLimitations(_GenType));
                else
                    GetLimitations((MemberInfo)tType);
            }
        }
        /// Determine limitations from type.
        private void GetLimitations(MemberInfo mMember)
        {
            // Determine limitations:
            lLimits.AddRange(mMember.GetCustomAttributes<LimitedInputAttribute>());
        }
        #endregion
        #region Management
        public void SetReadOnly()
        {
            ReadOnly = true;
        }
        #endregion
        #region Application
        public bool IsAllowed(object oValue)
        {
            foreach (var _lLimit in Limits)
            {
                switch (_lLimit)
                {
                    case AllowedInputAttribute _aAllowed:
                        if (!_aAllowed.Values.Contains(oValue))
                            return (false);
                        break;
                    case DeniedInputAttribute _dDenied:
                        if (_dDenied.Values.Contains(oValue))
                            return (false);
                        break;

                    default:
                        throw new NotImplementedException(string.Format("Not implemented limitation of type '{0}'!", _lLimit.GetType().Name));
                }
            }
            return (true);
        }
        #endregion
    }
    public class EnumeratedItemInfo : ObjectInfo
    {
        public EnumeratedItemInfo(ObjectInfo oOwner, int iIndex) : base(oOwner)
        {
            var _sVar = string.Format("[{0}]", iIndex);

            this.Index = iIndex;
            this.Title = "";
            this.Id += _sVar;
            this.Name += _sVar;

            if (Type == null)
                ;
            else if ((Type.IsGenericType) && (Type.GenericTypeArguments.First().GetPurpose() == ExtType.Purpose.ePrimitive))
            {
                // Determine mandatory attributes of owner:
                var _aAttrib = Property.GetCustomAttribute<CollectionItemDisplayNameAttribute>();

                this.Title = _aAttrib.FormatDisplayName(iIndex);
            }
        }

        #region Properties
        public int Index { private set; get; }
        #endregion


        #region Initialization
        protected override void CopyFrom(ObjectInfo oSource)
        {
            base.CopyFrom(oSource);

            if (oSource is EnumeratedItemInfo _eEnm)
            {
                this.Index = _eEnm.Index;
            }
        }
        #endregion
    }
    public abstract class DictionaryObjectInfo : ObjectInfo
    {
        protected DictionaryObjectInfo(ObjectInfo oOwner, int iIndex) : base(oOwner)
        {
            this.Index = iIndex;
        }

        #region Properties
        public int Index { private set; get; }
        #endregion


        #region Initialization
        protected override void CopyFrom(ObjectInfo oSource)
        {
            base.CopyFrom(oSource);

            if (oSource is DictionaryKeyInfo _eEnm)
            {
                this.Index = _eEnm.Index;
            }
        }
        #endregion
    }
    public class DictionaryKeyInfo : DictionaryObjectInfo
    {
        public DictionaryKeyInfo(ObjectInfo oOwner, int iIndex, object oKey) : base(oOwner, iIndex)
        {
            var _sVar = string.Format(".Keys[{0}]", iIndex);

            this.Id += _sVar;
            this.Name += _sVar;
            this.Title = oKey.ToString();
        }


        #region Properties.Management
        public override bool RenderControl { get { return (ReadOnly ? false : base.RenderControl); } }
        #endregion
    }
    public class DictionaryValueInfo : DictionaryObjectInfo
    {
        public DictionaryValueInfo(ObjectInfo oOwner, int iIndex, object oValue) : base(oOwner, iIndex)
        {
            var _sVar = string.Format(".Values[{0}]", iIndex);

            this.Id += _sVar;
            this.Name += _sVar;
            this.Title = oValue.ToString();
        }
    }
    public class ContentProviderInfo : ObjectInfo
    {
        #region Constants
        public const string KEY = "__CntPrv";
        #endregion


        public ContentProviderInfo(ObjectInfo oOwner, IContentProvider iProvider) : base(oOwner)
        {
            var _sVar = ("." + KEY);

            this.tType = iProvider.Content.GetType();
            this.Id += _sVar;
            this.Name += _sVar;
            this.Title = "";
        }


        #region Properties.Management
        public override PropertyInfo Property => null;
        public override Type Type => tType;
        private Type tType;
        #endregion
    }
    public class EnumeratedContentInfo : EnumeratedItemInfo
    {
        public EnumeratedContentInfo(ContentProviderInfo cOwner, object oInstance, int iIndex) : base(cOwner, iIndex)
        {
            this.tType = oInstance.GetType();
        }


        #region Properties.Management
        public override PropertyInfo Property => null;
        public override Type Type => tType;
        private Type tType;
        #endregion
    }
    #endregion


    #region Extensions
    public static class ExtType_
    {
        public static ExtType.Purpose GetPurposeEx(this Type tType)
        {
            var pPurpose = tType.GetPurpose();
            if (pPurpose == ExtType.Purpose.eClass)
            {
                // Exceptional types:
                if (tType.Equals(typeof(TimeSpan)))
                    return (ExtType.Purpose.ePrimitive);
                else if (tType.Equals(typeof(DateTime)))
                    return (ExtType.Purpose.ePrimitive);
            }
            return (pPurpose);
        }
    }
    #endregion
}