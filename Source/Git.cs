// Very simple git integration!
// For now, it just runs a git process to execute several commands.
// > Only a return code is evaluated to detect succeeded and failed comands.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Shared.Utils.Core;


namespace Shared.Source.Git
{
    public sealed class Git
    {
        #region Constants
        private const string GIT_FOLDER = ".git";
        private const string GIT_FILENAME = "git";
        #endregion


        #region Types
        public class Repository
        {
            public Repository(string sPath)
            {
                this.Path = sPath;
                this.Name = sPath.Split(System.IO.Path.DirectorySeparatorChar).Last();

                // Validate path:
                try
                {
                    GetStatus();
                }
                catch (Exception _eExcpt)
                {
                    throw new FileLoadException(string.Format("The folder '{0}' does not seem to be a git repository!", sPath), _eExcpt);
                }
            }


            #region Properties.Management
            public object Tag { set; get; }
            #endregion
            #region Properties
            public string Name { init; get; }
            public string Path { init; get; }
            #endregion


            #region Management.Commands
            public void GetStatus() => RunGitCommand(Path, "status");

            public void DiscardChanges() => RunGitCommand(Path, "clean -fdx");

            public void Pull() => RunGitCommand(Path, "pull");
            #endregion
            #region Management.Commands
            private static void RunGitCommands(string sRepositoryPath, params string[] sCommand)
            {
                AggregateException _aAggExcpt = null;
                foreach (var _sCmd in sCommand)
                {
                    try
                    {
                        RunGitCommand(sRepositoryPath, _sCmd);
                    }
                    catch (Exception _eExcpt)
                    {
                        if (_aAggExcpt == null)
                            _aAggExcpt = new AggregateException("Failed to execute git commands!");
                        
                        _aAggExcpt.InnerExceptions.Append(_eExcpt);
                    }              
                }
                if (_aAggExcpt != null)
                    throw _aAggExcpt;
            }
            /// <summary>
            /// Runs a git command.
            /// </summary>
            /// <typeparam name="sRepositoryPath">Path to repository.</typeparam>
            /// <typeparam name="sCommand">Command to run.</typeparam>
            private static void RunGitCommand(string sRepositoryPath, string sCommand)
            {
                pProcInfo.WorkingDirectory = sRepositoryPath;
                pProcInfo.Arguments = sCommand;
                var _pProc = new Process { StartInfo = pProcInfo };
                {
                    _pProc.Start();
                    _pProc.WaitForExit();

                    string _sOut = _pProc.StandardOutput.ReadToEnd();
                    Debug.WriteLine(_sOut);

                    if (_pProc.ExitCode != 0)
                        throw new Exception(string.Format("Git command '{0}' failed with code {1}!", sCommand, _pProc.ExitCode), new OperationCanceledException(_sOut));
                }
            }
            private static ProcessStartInfo pProcInfo = new ProcessStartInfo {
                FileName = GIT_FILENAME,
                CreateNoWindow = true,
                RedirectStandardOutput = true
            };
            #endregion


            public override string ToString() => Name;
        }
        #endregion


        #region Management.Repository
        /// <summary>
        /// Opens an existing repostory.
        /// </summary>
        /// <param name="sPath">Path to repository.</param>
        public static Repository OpenRepository(string sPath) => OpenRepository<Repository>(sPath);
        /// <summary>
        /// Opens an existing repostory.
        /// </summary>
        /// <remarks>
        /// Use this method to create a subclassed repository type.
        /// </remarks>
        /// <param name="sPath">Path to repository.</param>
        /// <typeparam name="TRepo">Subclassed repository type to return.</typeparam>
        public static TRepo OpenRepository<TRepo>(string sPath)
            where TRepo : Repository
        {
            return ((TRepo)Activator.CreateInstance(typeof(TRepo), new object[] { sPath }));
        }
        /// <summary>
        /// Enumerates all repositories in a certain path.
        /// </summary>
        /// <param name="sPath">Path to search for existing repositoes.</param>
        public static IEnumerable<Repository> EnumerateReporitories(string sPath) => EnumerateReporitories<Repository>(sPath);
        /// <summary>
        /// Enumerates all repositories in a certain path.
        /// </summary>
        /// <remarks>
        /// Use this method to create a subclassed repository type.
        /// </remarks>
        /// <param name="sPath">Path to search for existing repositoes.</param>
        /// <typeparam name="TRepo">Subclassed repository type to return.</typeparam>
        public static IEnumerable<TRepo> EnumerateReporitories<TRepo>(string sPath)
            where TRepo : Repository
        {
            return (Directory
                .EnumerateDirectories(sPath, GIT_FOLDER, SearchOption.AllDirectories)
                .Select(_sPath => OpenRepository<TRepo>(Path.GetDirectoryName(_sPath.RemoveEnd(GIT_FOLDER)))));
        }
        #endregion
    }
}