using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace Shared.Collections.Exposed
{
    internal abstract class ExposedReadOnlyKeyValueEnumeration<TKey, TValue, TExpValue> : IEnumerable<KeyValuePair<TKey, TExpValue>>
        where TKey : notnull
        where TValue : TExpValue
    {
        #region Types
        /// <summary>
        /// Converty from values from <see cref="TValue"/> to exposed type <see cref="TExpValue"/>.
        /// </summary>    
        private class KeyValueEnumerator : IEnumerator<KeyValuePair<TKey, TExpValue>>
        {
            public KeyValueEnumerator(IEnumerator<KeyValuePair<TKey, TValue>> enumerator)
            {
                this.enumerator = enumerator;
            }


            #region Properties.Compatibility
            public KeyValuePair<TKey, TExpValue> Current => new KeyValuePair<TKey, TExpValue>(enumerator.Current.Key, enumerator.Current.Value);
            object IEnumerator.Current => enumerator.Current;
            #endregion


            #region Compatibility
            public void Dispose() => enumerator.Dispose();
            public bool MoveNext() => enumerator.MoveNext();
            public void Reset() => enumerator.Reset();
            #endregion


            private IEnumerator<KeyValuePair<TKey, TValue>> enumerator;
        }
        #endregion


        /// <summary>
        /// Construct with external enumerable type.
        /// </summary>
        protected ExposedReadOnlyKeyValueEnumeration(IEnumerable<KeyValuePair<TKey, TValue>> enumerable)
        {
            this.Enumerable = enumerable;
        }


        #region Properties.Management
        protected IEnumerable<KeyValuePair<TKey, TValue>> Enumerable { init; get; }
        #endregion


        #region Compatibility
        public IEnumerator<KeyValuePair<TKey, TExpValue>> GetEnumerator()
        {
            if (Enumerable == null)
                return (Empty());
            else
                return (new KeyValueEnumerator(Enumerable.GetEnumerator()));
        }
        IEnumerator IEnumerable.GetEnumerator() => Enumerable.GetEnumerator();
        #endregion


        #region Helper
        private IEnumerator<KeyValuePair<TKey, TExpValue>> Empty()
        {
            yield break;
        }
        #endregion
    }

    /// <summary>
    /// Represents a dictionary of keys and values where the values are exposed by an abstract type.
    /// </summary>
    /// <remarks>
    /// <b>Performance</b> is about <i>4 times</i> slower compared to standard <see cref="IReadOnlyDictionary"/>!
    /// </remarks>
    /// <example>
    /// private Dictionary<int, MyType> myDict = new ();
    /// private class MyType : IDisposable { }
    /// public IReadOnlyDictionary<int, IDisposable> Dict = new ExposedReadOnlyDictionary<int, MyType, IDisposable>(myDict);
    /// </example>
    internal class ExposedReadOnlyDictionary<TKey, TValue, TExpValue> : ExposedReadOnlyKeyValueEnumeration<TKey, TValue, TExpValue>, IReadOnlyDictionary<TKey, TExpValue>
        where TKey : notnull
        where TValue : TExpValue
    {
        /// <summary>
        /// Construct with new instance of internal dictionary.
        /// </summary>
        protected ExposedReadOnlyDictionary() : base(null)
        {
            this.Dictionary = new Dictionary<TKey, TValue>();
            this.Enumerable = Dictionary;
        }
        /// <summary>
        /// Construct with external dictionary.
        /// </summary>
        public ExposedReadOnlyDictionary(IDictionary<TKey, TValue> dictionary) : base(dictionary)
        {
            this.Dictionary = dictionary;
        }


        #region Properties.Compatibility
        public IEnumerable<TKey> Keys
        {
            get
            {
                if (Dictionary == null)
                    yield break;
                else
                {
                    foreach (var _tKey in Dictionary.Keys)
                        yield return (_tKey);
                }
            }
        }
        public IEnumerable<TExpValue> Values
        {
            get
            {
                if (Dictionary == null)
                    yield break;
                else
                {
                    foreach (var _tVal in Dictionary.Values)
                        yield return ((TExpValue)_tVal);
                }
            }
        }
        public int Count => Dictionary.Count;

        public TExpValue this[TKey key] => Dictionary[key];
        #endregion
        #region Properties.Management
        protected IDictionary<TKey, TValue> Dictionary { init; get; }
        #endregion


        #region Compatibility
        public bool ContainsKey(TKey key) => Dictionary.ContainsKey(key);
        public bool TryGetValue(TKey key, [MaybeNullWhen(false)] out TExpValue value)
        {
            bool bResult;
            if (bResult = Dictionary.TryGetValue(key, out var val))
                value = val;
            else
                value = default(TExpValue);
            return (bResult);
        }
        #endregion
    }
    /// <summary>
    /// Represents a keyed collection where the items are exposed by an abstract type.
    /// </summary>
    /// <remarks>
    /// <b>Performance</b> is about <i>5 times</i> slower compared to standard <see cref="IReadOnlyDictionary"/>!
    /// </remarks>
    /// <example>
    /// private class MyCollection : KeyedCollection<int, MyType> { }
    /// private MyCollection myCol = new ();
    /// private class MyType : IDisposable { }
    /// public IReadOnlyDictionary<int, IDisposable> Dict = new ExposedReadOnlyKeyedCollection<int, MyType, IDisposable>(myCol);
    /// </example>
    internal class ExposedReadOnlyKeyedCollection<TKey, TValue, TExpValue> : ExposedReadOnlyDictionary<TKey, TValue, TExpValue>
        where TKey : notnull
        where TValue : TExpValue
    {
        public ExposedReadOnlyKeyedCollection(KeyedCollection<TKey, TValue> collection) : base(null)
        {
            var _dict = typeof(KeyedCollection<TKey, TValue>).GetProperty("Dictionary", BindingFlags.NonPublic | BindingFlags.Instance);
            var _get = _dict.GetGetMethod(true);

            this.Dictionary = (IDictionary<TKey, TValue>)_get.Invoke(collection, null);
            this.Enumerable = Dictionary;
        }
    }
}
