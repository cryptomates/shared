// Displays an alert to the user.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using Shared.Utils.Asp;

namespace Shared.Asp.Alert
{
    #region Attributes
    public class AlertLayoutAttribute : Attribute
    {
        public AlertLayoutAttribute(Shared.Asp.Alert.AlertLayout aLayout, AlertIcon aIcon)
        {
            this.Layout = aLayout;
            this.Icon = aIcon;
        }


        #region Properties
        public Shared.Asp.Alert.AlertLayout Layout { get; }
        public AlertIcon Icon { get; }
        #endregion
    }
    #endregion
    #region Enumerations
    public enum AlertLayout
    {
        [Description("primary")]
        ePrimary,
        [Description("secondary")]
        eSecondary,
        [Description("success")]
        eSuccess,
        [Description("danger")]
        eDanger,
        [Description("warning")]
        eWarning,
        [Description("info")]
        eInfo,
        [Description("light")]
        eLight,
        [Description("dark")]
        eDark
    }
    public enum AlertIcon
    {
        None,
        Empty,

        eCheck,
        eInfo,
        eExclamation,
    }
    #endregion


    #region Interfaces
    /// Implement to provide an alert list.
    public interface IAlertProvider
    {
        #region Properties
        IEnumerable<Alert> Alerts { get; }
        #endregion
    }
    #endregion


    #region Types.Settings
    public class AlertSettings
    {
        public AlertSettings(IEnumerable<Alert> aModel)
        {
            this.Model = aModel;
        }


        #region Properties.Management
        public IEnumerable<Alert> Model { init; get; }
        #endregion
    }
    #endregion
    #region Types
    public interface IAlertItem
    {
        #region Properties.Management
        bool Visible { set; get; }
        object Tag { set; get; }
        #endregion
    }
    public class AlertSeparatorItem : IAlertItem
    {
        #region Properties.Compatibility
        object IAlertItem.Tag { set; get; }
        #endregion
        #region Properties.Management
        public bool Visible { set; get; } = true;
        #endregion
    }
    public class AlertTextItem : IAlertItem
    {
        public AlertTextItem(string sText, AlertIcon aIcon = AlertIcon.None)
        {
            this.Icon = aIcon;
            this.Text = sText;
        }


        #region Properties.Management
        public bool Visible { set; get; } = true;
        public object Tag { set; get; }
        #endregion
        #region Properties
        public AlertIcon Icon { set; get; }
        public string Text { set; get; }
        #endregion


        public override string ToString() => Text;
    }
    public class AlertTitleItem : AlertTextItem
    {
        public AlertTitleItem(string sText, AlertIcon aIcon = AlertIcon.None) : base(sText, aIcon)
        {
        }
    }
    public class Alert
    {
        public Alert(AlertLayout aLayout, params IAlertItem[] iItems)
        {
            this.Layout = aLayout;
            this.Items.AddRange(iItems);
        }
        public Alert(string sText, AlertIcon aIcon = AlertIcon.eInfo, AlertLayout aLayout = AlertLayout.ePrimary) : this(aLayout, new AlertTextItem(sText, aIcon))
        {
        }
        public Alert(string sTitle, string sText, AlertIcon aIcon = AlertIcon.eInfo, AlertLayout aLayout = AlertLayout.ePrimary) : this(aLayout, new AlertTitleItem(sTitle, aIcon), new AlertTextItem(sText))
        {
        }


        #region Properties.Management
        public bool Visible { set; get; } = true;
        public object Tag { set; get; }
        #endregion
        #region Properties
        public AlertLayout Layout { set; get; }
        public List<IAlertItem> Items { get; } = new List<IAlertItem>();
        #endregion
    }
    #endregion
}