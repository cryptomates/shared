using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Text;
using Shared.Utils.Core;

namespace Shared.IO
{
    #region Structures
    internal struct FileHeader
    {
        #region Properties
        public int Version { set; get; }
        public int PackageCount { set; get; }
        #endregion


        #region Management
        public void Write(FileStream fStream)
        {
            using (BinaryWriter bWriter = new BinaryWriter(fStream, PackageFileStream.ENCODING, true))
            {
                bWriter.Write(Version);
                bWriter.Write(PackageCount);
            }
        }
        public static FileHeader Read(FileStream fStream)
        {
            using (BinaryReader bReader = new BinaryReader(fStream, PackageFileStream.ENCODING, true))
            {
                return (new FileHeader()
                {
                    Version = bReader.ReadInt32(),
                    PackageCount = bReader.ReadInt32()
                });
            }
        }
        #endregion
    }
    internal struct PackageHeader
    {
        public PackageHeader(Package pPackage)
        {
            this.IsCompressed = pPackage.IsCompressed;
            this.Length = (int)pPackage.CurrentLength;
            this.Identifier = pPackage.Identifier;
        }


        #region Properties
        public bool IsCompressed { private set; get; }
        public int Length { private set; get; }
        public int Identifier { private set; get; }
        #endregion


        #region Management
        public void Write(Stream fStream)
        {
            using (BinaryWriter bWriter = new BinaryWriter(fStream, PackageFileStream.ENCODING, true))
            {
                bWriter.Write(IsCompressed);
                bWriter.Write(Length);
                bWriter.Write(Identifier);
            }
        }
        public static PackageHeader Read(Stream fStream)
        {
            using (BinaryReader bReader = new BinaryReader(fStream, PackageFileStream.ENCODING, true))
            {
                return (new PackageHeader()
                {
                    IsCompressed = bReader.ReadBoolean(),
                    Length = bReader.ReadInt32(),
                    Identifier = bReader.ReadInt32()
                });
            }
        }
        #endregion
    }
    #endregion

    #region Types
    /// <summary>
    /// Content-Package.
    /// </summary>
    public class Package : IDisposable
    {
        /// <summary>
        /// Create package for Write-operations.
        /// </summary>
        public Package(bool bCompress = false, int iIdentifier = 0)
        {
            this.Identifier = iIdentifier;

            if (bCompress)
                dCompress = new DeflateStream(mStream, CompressionMode.Compress, true);
        }
        /// <summary>
        /// Create package for Read-operations from a stream.
        /// </summary>
        internal Package(Stream fStream)
        {
            // Read header:
            var pHeader = PackageHeader.Read(fStream);

            // Initialize:
            Identifier = pHeader.Identifier;

            // Read content into packages memory stream:
            if (pHeader.IsCompressed)
            {
                using (var _lLmStream = new LimitedStream(fStream, pHeader.Length))
                using (var _dCprsStream = new DeflateStream(_lLmStream, CompressionMode.Decompress))
                    _dCprsStream.CopyTo(CurrentStream);
            }
            else
                fStream.CopyTo(CurrentStream, pHeader.Length, PackageFileStream.READ_BUFFER_LENGTH);

            // Prepare for reading data from outside:
            CurrentStream.Position = 0;
        }


        #region Properties.Management
        public bool IsValid { get { return (CurrentStream != null); } }
        public bool IsCompressed { get { return (dCompress != null); } }
        public Stream? CurrentStream { get { return (IsCompressed ? (Stream)dCompress : (Stream)mStream); } }
        public long CurrentLength { get { return ((mStream == null) ? 0 : mStream.Length); } }
        #endregion
        #region Properties
        public int Identifier { set; get; }
        public Object Tag { set; get; }
        #endregion


        public virtual void Dispose()
        {
            if (dCompress != null)
                dCompress.Dispose();
            if (mStream != null)
                mStream.Dispose();
        }


        #region Management
        public void Close()
        {
            // Close package:
            if (dCompress != null)
            {
                dCompress.Close();
                dCompress = null;
            }
            mStream.Close();
            mStream = null;
        }
        internal virtual void Write(FileStream fStream)
        {
            if (IsValid)
            {
                // Close compressed stream:
                if (IsCompressed)
                    dCompress.Close();

                // Write header:
                new PackageHeader(this).Write(fStream);

                // Write content:
                mStream.Flush();

                mStream.Position = 0;
                mStream.CopyTo(fStream);

                Close();
            }
            else
                throw new ObjectDisposedException("Package has been closed!");
        }
        #endregion


        private MemoryStream mStream = new MemoryStream();
        private DeflateStream dCompress = null;
    }
    public class PackageFileStream
    {
        #region Constants
        public const int VERSION_CURRENT = 1;
        public static readonly Encoding ENCODING = Encoding.UTF8;
        internal const int READ_BUFFER_LENGTH = 1024;
        #endregion


        #region Properties.Management
        public List<Package> Packages { get; } = new List<Package>();
        #endregion


        #region Subclassing
        protected virtual Package CreatePackage(FileStream fStream)
        {
            return (new Package(fStream));
        }
        #endregion
        #region Management
        public Package GetPackage(int iIdentifier)
        {
            return (Packages.First(_pPackage => (_pPackage.Identifier == iIdentifier)));
        }
        #endregion
        #region IO
        public void WritePackages(string sFilePath, FileMode fMode = FileMode.Create)
        {
            using (FileStream fStream = new FileStream(sFilePath, fMode, FileAccess.Write))
                WritePackages(fStream);
        }
        public void WritePackages(FileStream fStream)
        {
            // Write header:
            new FileHeader()
            {
                Version = VERSION_CURRENT,
                PackageCount = Packages.Count
            }.Write(fStream);

            // Write contents:
            foreach (var _pPack in Packages)
                _pPack.Write(fStream);
        }
        /// <summary>
        /// Read packages from a file.
        /// </summary>
        /// <param name="sFilePath"></param>
        /// <param name="iMax">Max. amount of packages to read.</param>
        public void ReadPackages(string sFilePath, int iMax = 0)
        {
            using (FileStream fStream = new FileStream(sFilePath, FileMode.Open, FileAccess.Read))
                ReadPackages(fStream, iMax);
        }
        /// <summary>
        /// Read packages from a stream.
        /// </summary>
        /// <param name="fStream"></param>
        /// <param name="iMax">Max. amount of packages to read.</param>
        public void ReadPackages(FileStream fStream, int iMax = 0)
        {
            Packages.Clear();

            // Read header:
            var _fHeader = FileHeader.Read(fStream);

            // Read contents:
            if (iMax > 0)
                _fHeader.PackageCount = Math.Min(iMax, _fHeader.PackageCount);
            for (int i = 0; i < _fHeader.PackageCount; i++)
                Packages.Add(CreatePackage(fStream));
        }

        /// <summary>
        /// Read packages from a file.
        /// </summary>
        /// <param name="sFilePath"></param>
        /// <param name="iIndentifiers">Array of identifiers to specify to read packages.</param>
        public void ReadPackages(string sFilePath, int[] iIndentifiers)
        {
            using (FileStream fStream = new FileStream(sFilePath, FileMode.Open, FileAccess.Read))
                ReadPackages(fStream, iIndentifiers);
        }
        /// <summary>
        /// Read packages from a stream.
        /// </summary>
        /// <param name="fStream"></param>
        /// <param name="iIndentifiers">Array of identifiers to specify to read packages.</param>
        public void ReadPackages(FileStream fStream, int[] iIndentifiers)
        {
            Packages.Clear();

            // Read header:
            var _fHeader = FileHeader.Read(fStream);

            // Read contents:
            // (BETA) ... TO-IMPLEMENT:
            // Do not read all packages and add conditionally!
            // > Read only package headers. Read content only if identifier is requested.
            for (int i = 0; i < _fHeader.PackageCount; i++)
            {
                var _pPackage = CreatePackage(fStream);
                if (iIndentifiers.Contains(_pPackage.Identifier))
                    Packages.Add(_pPackage);
            }

            if (Packages.Count != iIndentifiers.Count())
                throw new InvalidDataException("Failed to read all requested packages!");
        }
        #endregion
    }
    /// <summary>
    /// Content-Package with an included (reader- or writer-) modificator instance.
    /// </summary>
    /// <typeparam name="TMod">Type of modificator instance.</typeparam>
    public class Package<TMod> : Package where TMod : IDisposable
    {
        public Package(bool bCompress = false, int iIdentifier = 0) : base(bCompress, iIdentifier)
        {
            Init();
        }
        internal Package(Stream fStream) : base(fStream)
        {
            Init();
        }


        #region Properties
        public TMod Modifier { private set; get; }
        #endregion


        public override void Dispose()
        {
            Modifier.Dispose();
            base.Dispose();
        }


        #region Initialization
        private void Init()
        {
            Modifier = (TMod)Activator.CreateInstance(typeof(TMod), new object[] { CurrentStream, PackageFileStream.ENCODING, true });
        }
        #endregion
        #region Management
        internal override void Write(FileStream fStream)
        {
            // Close writer if 'Modifier' is a writer-type:
            MethodInfo _mClose = Modifier.GetType().GetMethod("Close");
            if (_mClose != null)
                _mClose.Invoke(Modifier, new object[] { });

            base.Write(fStream);
        }
        #endregion
    }
    public class PackageFileStream<TMod> : PackageFileStream where TMod : IDisposable
    {
        #region Properties.Management
        public new List<Package<TMod>> Packages { get { return (base.Packages.ConvertAll(_pPack => { return ((Package<TMod>)_pPack); })); } }
        #endregion


        #region Subclassing
        protected override Package CreatePackage(FileStream fStream)
        {
            return (new Package<TMod>(fStream));
        }
        #endregion
        #region Management
        public new Package<TMod> GetPackage(int iIdentifier)
        {
            return ((Package<TMod>)base.GetPackage(iIdentifier));
        }
        #endregion
    }
    #endregion
}