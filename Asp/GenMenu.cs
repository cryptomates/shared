// [Generic menu]
// - Description:
//   Provides *.cshtml based generic menu for C# types.
// - Usage:
//   - 1) MyClass.cs:
//     public class MyClass : IMenuProvider                                         // Provide menu.
//     {
//          public MyClass()
//          {
//              MenuButton _aBut;
//              var _aGen = new MenuGroup("General");                               // Build menu ...
//              {
//                  _aGen.MenuButtons.Add(_aBut = new MenuButton("Test"));
//                  _aBut.Click += OnTest;
//              }
//              lMenuGroups.Add(_aGen);
//          }
//
//          public IEnumerable<IMenuGroup> MenuGroups => lMenuGroups;               // Expose menu.
//          private List<IMenuGroup> lMenuGroups = new List<IMenuGroup>();
//     }

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Shared.Asp.GenNav;


namespace Shared.Asp.GenMenu
{
    #region Enumerations
    public enum MenuGroupLayout
    {
        eSmall,
        eMedium,
        eLarge
    }
    public enum MenuButtonLayout
    {
        [Description("btn-primary")]
        ePrimary,
        [Description("btn-secondary")]
        eSecondary,
        [Description("btn-success")]
        eSuccess,
        [Description("btn-danger")]
        eDanger,
        [Description("btn-warning")]
        eWarning,
        [Description("btn-info")]
        eInfo,
        [Description("btn-light")]
        eLight,
        [Description("btn-dark")]
        eDark,
        [Description("btn-link")]
        eLink
    }
    #endregion


    #region EventArguments
    public class MenuEventArgs : EventArgs
    {
        #region Constants
        public static readonly new MenuEventArgs Empty = new MenuEventArgs(null);
        #endregion


        public MenuEventArgs(IMenuItem iItem)
        {
            this.Item = iItem;
        }


        #region Properties.Management
        public IMenuItem Item { init; get; }
        #endregion
    }
    #endregion


    #region Interfaces
    /// Implement to provide menu.
    public interface IMenuProvider
    {
        #region Properties
        IEnumerable<IMenuGroup> MenuGroups { get; }
        #endregion


        #region Management
        public IMenuItem FindMenuItem(ulong ulId) => MenuGroups.FindItem(ulId);
        #endregion
    }
    #endregion


    #region Types.Groups
    public interface IMenuGroup : IMenuItem, IEnumerable<IMenuButton>
    {
        #region Properties.Management
        MenuGroupLayout Layout { get; }
        
        IList<IMenuButton> MenuButtons { get; }
        #endregion
        #region Properties
        public string Label { set; get; }
        #endregion
    }
    public class MenuGroup : MenuItem, IMenuGroup
    {
        public MenuGroup(string sLabel = "", MenuGroupLayout aLayout = MenuGroupLayout.eMedium, string sAccess = "") : base(sAccess)
        {
            this.Label = sLabel;
            this.Layout = aLayout;
        }


        #region Properties.Management
        public MenuGroupLayout Layout { init; get; }
        public IList<IMenuButton> MenuButtons { init; get; } = new List<IMenuButton>();
        #endregion
        #region Properties
        public string Label { set; get; }
        #endregion


        public IEnumerator<IMenuButton> GetEnumerator() => MenuButtons.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => MenuButtons.GetEnumerator();
    }
    #endregion
    #region Types.Buttons
    public interface IMenuButton : IMenuItem
    {
        #region Properties
        MenuButtonLayout Layout { set; get; }
        bool Outlined { set; get; }
        #endregion
    }
    /// Menu-button.
    public class MenuButton : TextMenuItem, IMenuButton
    {
        public MenuButton(string sText = "", MenuButtonLayout aLayout = MenuButtonLayout.ePrimary, bool bOutlined = false, string sAccess = "") : base(sText, sAccess)
        {
            this.Layout = aLayout;
            this.Outlined = bOutlined;

            if ((bOutlined) && (aLayout == MenuButtonLayout.eLink))
                throw new NotSupportedException("Unable to combine linked with outlined button!");
        }


        #region Properties
        public MenuButtonLayout Layout { set; get; }
        public bool Outlined { set; get; }
        #endregion
    }
    /// Split menu-button.
    public class SplitMenuButton : MenuButton, IEnumerable<IMenuItem>
    {
        public SplitMenuButton(string sText = "", MenuButtonLayout aLayout = MenuButtonLayout.ePrimary, bool bOutlined = false, string sAccess = "") : base(sText, aLayout, bOutlined, sAccess)
        {
        }


        #region Properties.Management
        public IList<IMenuItem> Items { get; } = new List<IMenuItem>();
        #endregion


        public IEnumerator<IMenuItem> GetEnumerator() => Items.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }
    /// Drop-down menu-button.
    public class DropDownMenuButton : SplitMenuButton
    {
        public DropDownMenuButton(string sText = "", MenuButtonLayout aLayout = MenuButtonLayout.ePrimary, bool bOutlined = false, string sAccess = "") : base(sText, aLayout, bOutlined, sAccess)
        {
        }
    }
    #endregion
    #region Types.Items
    public interface IMenuItem
    {
        #region Properties.Management
        public ulong Id { get; }
        string Access { get; }
        object Tag { set; get; }
        #endregion
        #region Properties
        bool Enabled { set; get; }
        bool Visible { set; get; }
        #endregion


        #region Management
        public IMenuItem FindItem(ulong ulId)
        {
            if (ulId == Id)
                return (this);
            else if (this is IEnumerable<IMenuItem> _iEnum)
                return (_iEnum.FindItem(ulId));
            else
                return (null);
        }
        #endregion
    }
    /// Textual menu item.
    public abstract class MenuItem : IMenuItem
    {
        public MenuItem(string sAccess = "")
        {
            this.Id = ulId++;
            this.Access = sAccess;
        }


        #region Properties.Management
        /// Unique, auto-generated menu ID.
        public ulong Id { get; }
        /// Min. user's required role to gain access.
        public string Access { get; }
        public object Tag { set; get; }
        #endregion
        #region Properties
        public bool Enabled { set; get; } = true;
        public bool Visible { set; get; } = true;
        #endregion


        private static ulong ulId = 100;
    }
    /// Separator menu item.
    public class MenuSeparatorItem : IMenuItem
    {
        #region Properties.Compatibility
        ulong IMenuItem.Id => 0;
        string IMenuItem.Access => "";
        object IMenuItem.Tag { set; get; }
        #endregion
        #region Properties
        public bool Enabled { set; get; } = true;
        public bool Visible { set; get; } = true;
        #endregion
    }
    /// Header menu item.
    public class HeaderMenuItem : MenuItem
    {
        public HeaderMenuItem(string sText = "", string sAccess = "") : base(sAccess)
        {
            this.Text = sText;
        }


        #region Properties
        public string Text { set; get; }
        #endregion


        public override string ToString() => Text;
    }
    /// Textual menu item.
    public class TextMenuItem : HeaderMenuItem
    {
        public TextMenuItem(string sText = "", string sAccess = "") : base(sText, sAccess)
        {
        }


        #region Events
        public event EventHandler<MenuEventArgs> Click;
        internal void _Click(object? oSender) => Click?.Invoke(oSender, new MenuEventArgs(this));
        #endregion
    }
    #endregion


    #region Types.Actions
    public class ButtonClickMenuAction : NavAction
    {
        #region Constants
        public const string KEY = "buttonClick";
        #endregion


        public ButtonClickMenuAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
            this.ItemId = ulong.Parse(sValue);
        }


        #region Properties.Management
        public ulong ItemId { init; get; }
        #endregion


        #region Format
        public static string FormatName(ulong ultemId) => Format(KEY, ultemId);
        #endregion
    }
    #endregion


    #region Extensions
    public static class ExtGenMenu
    {
        public static IMenuItem FindItem(this IEnumerable<IMenuItem> iSource, ulong ulId)
        {
            foreach (var _iItem in iSource)
            {
                var _iResult = _iItem.FindItem(ulId);
                if (_iResult != null)
                    return (_iResult);
            }
            return (null);
        }
    }
    #endregion
}