using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Shared.Net
{
    public class WebRequestEx
    {
        // Konstanten:
        // --------------------------------------------------------------------------------------
        public static int MIN_CONTENT_LENGTH = 3;                                                                       // L�nge, ab der empfangene Inhalte f�r G�ltig befunden werden.


        // Enumerationen:
        // --------------------------------------------------------------------------------------
        public enum Result
        {
            Skipped,
            Success,
            Error
        }


        // Datentypen:
        // --------------------------------------------------------------------------------------
        public class ErrorData
        {
            public ErrorData(Exception eExcption)
            {
                this.sMessage = eExcption.Message;
                this.iHResult = eExcption.HResult;

                Debug.WriteLine("[ED38] Error-Code: 0x{0:X} ({1})", iHResult, sMessage);
            }


            public int iHResult;
            public String sMessage;
        }


        public WebRequestEx(String sURI = "", TimeSpan tDelay = default(TimeSpan))
        {
            this.URI = sURI;
            this.Delay = tDelay;
        }


        // Eigenschaften:
        // --------------------------------------------------------------------------------------
        #region Properties.Config
        [Browsable(false)]
        public String URI { set; get; }
        [Browsable(false)]
        public bool IsValid { get { return (URI.Length > 0); } }
        [DisplayName("Ergebnis der letzten Operation.")]
        [Browsable(false)]
        public Result LastResult { protected set; get; } = Result.Error;
        [DisplayName("Details des zuletzt aufgetretenen Fehlers.")]
        [Browsable(false)]
        public ErrorData LastError { protected set; get; }
        [DisplayName("Gib an ob der Inhalt der letzten Anwendung auszuwerten ist.")]
        [Browsable(false)]
        public bool IsEvaluable { get { return ((LastResult == Result.Success) && (IsEvaluated == false)); } }
        [DisplayName("Gib an ob der Inhalt der letzten Anwendung ausgewertet ist.")]
        [Browsable(false)]
        public bool IsEvaluated { set; get; }
        [DisplayName("Status der letzten Operation.")]
        [Browsable(false)]
        public String Status { private set; get; }
        [DisplayName("Empfangener Inhalt der letzten Operation.")]
        [Browsable(false)]
        public String Received { private set; get; }
        [DisplayName("Minimaler zeitlicher Abstand zwischen aufeinanderfolgenden Aufrufen von 'GetContent()'.")]
        [Browsable(false)]
        public TimeSpan Delay { set; get; }
        #endregion


        // Management:
        // --------------------------------------------------------------------------------------
        public virtual async Task<Result> GetContentAsync()
        {
            LastResult = Result.Error;
            LastError = null;

            // Ggf. pr�fen ob minimale Pausendauer eingehalten wird:
            if ((Delay == TimeSpan.Zero) || ((DateTime.Now - sLastRun) >= Delay))
            {
                if (IsValid)
                {
                    try
                    {
                        System.Net.WebRequest wRequest = System.Net.WebRequest.Create(URI);
                        WebResponse wResponse = null;

                        // Antwort ermitteln:
                        wRequest.Proxy = null;

                        await Task.Run(() => wResponse = wRequest.GetResponse());

                        // Status ermitteln:
                        Status = ((HttpWebResponse)wResponse).StatusDescription;

                        // Inhalt auslesen:
                        Stream sDataStream = wResponse.GetResponseStream();
                        StreamReader sReader = new StreamReader(sDataStream);

                        Received = sReader.ReadToEnd();

                        // Speicher freigeben:
                        sReader.Close();
                        sDataStream.Close();
                        wResponse.Close();

                        // Pr�fen ob Inhal eine f�r G�ltig befundene L�nge ausweist:
                        if (Received.Length > MIN_CONTENT_LENGTH)
                        //if (sStatus == "OK")
                        {
                            // Letzten Ausf�hrungszeitpunkt speichern:
                            sLastRun = DateTime.Now;

                            IsEvaluated = false;
                            LastResult = Result.Success;
                        }
                        else
                            throw new WebException(String.Format("[GC161] Invalid length of reseived data '{0}'", Received));
                    }
                    catch (Exception eExcpt)
                    {
                        LastError = new ErrorData(eExcpt);
                    }
                }
                else
                    Debug.Assert(false, "[GC124]");
            }
            else
                LastResult = Result.Skipped;

            return (LastResult);
        }


        private DateTime sLastRun;
    }
}