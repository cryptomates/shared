﻿// Simple HTTP Server in C#
// https://www.codeproject.com/Articles/137979/Simple-HTTP-Server-in-C
// HTTP explanation
// http://www.jmarshall.com/easy/http/

using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Shared.Utils.Core;

namespace Shared.Net
{
    public class HttpProcessor
    {
        // Konstanten:
        // ---------------------------------------------------------------------------
        private const int BUF_SIZE = 4096;
        private static int MAX_POST_SIZE = (10 * 1024 * 1024);          // 10 MB


        public TcpClient tSocket;
        public HttpServer hServer;

        private Stream sInput;
        public StreamWriter sOutput;

        public String sHTTP_Method;
        public String sHTTP_URL;
        public String sHTTP_ProtocolVersionString;
        public Hashtable hHTTP_Headers = new Hashtable();


        public HttpProcessor(TcpClient tClient, HttpServer hServer)
        {
            this.tSocket = tClient;
            this.hServer = hServer;
        }


        // Management:
        // ---------------------------------------------------------------------------
        public void Process()
        {
            // we can't use a StreamReader for input, because it buffers up extra data on us inside it's
            // "processed" view of the world, and we want the data raw after the headers
            sInput = new BufferedStream(tSocket.GetStream());

            // we probably shouldn't be using a streamwriter for all output from handlers either
            sOutput = new StreamWriter(new BufferedStream(tSocket.GetStream()));

            try
            {
                ParseRequest();
                ReadHeaders();

                if (sHTTP_Method.Equals("GET"))
                    OnHandleGETRequest();

                else if (sHTTP_Method.Equals("POST"))
                    OnHandlePOSTRequest();
            }
            catch (Exception eExcpt)
            {
                Console.WriteLine("Exception: " + eExcpt.ToString());
                WriteFailure();
            }

            try
            {
                // Kann IO-Exception auslösen wenn Client-Socket bereits geschlossen wurde:
                sOutput.Flush();
                // bs.Flush(); // flush any remaining output
            }
            catch (IOException iExcpt)
            {
            }

            sInput = null;
            sOutput = null; // bs = null;    

            tSocket.Close();
        }


        // Commands:
        // ---------------------------------------------------------------------------
        public void WriteSuccess(string content_type = "text/html")
        {
            sOutput.WriteLine("HTTP/1.0 200 OK");
            sOutput.WriteLine("Content-Type: " + content_type);
            sOutput.WriteLine("Connection: close");
            sOutput.WriteLine("");
        }
        public void WriteFailure()
        {
            sOutput.WriteLine("HTTP/1.0 404 File not found");
            sOutput.WriteLine("Connection: close");
            sOutput.WriteLine("");
        }
        public void PrintTaggedLine(String sTag, String sContent, String sTagAttributes = "")
        {
            if (sTagAttributes.Length > 0)
                // Führendes Leerzeichen einfügen:
                sTagAttributes = (" " + sTagAttributes);

            sOutput.WriteLine(String.Format("<{0}{2}>{1}</{0}>", sTag, sContent, sTagAttributes));
        }


        // Management:
        // ---------------------------------------------------------------------------
        private void ParseRequest()
        {
            String request = StreamReadLine(sInput);
            string[] tokens = request.Split(' ');

            if (tokens.Length != 3)
                throw new Exception("invalid http request line");

            sHTTP_Method = tokens[0].ToUpper();
            sHTTP_URL = tokens[1];
            sHTTP_ProtocolVersionString = tokens[2];

            Console.WriteLine("starting: " + request);
        }

        private void ReadHeaders()
        {
            Console.WriteLine("readHeaders()");
            String line;

            while ((line = StreamReadLine(sInput)) != null)
            {
                if (line.Equals(""))
                {
                    Console.WriteLine("got headers");
                    return;
                }

                int separator = line.IndexOf(':');
                if (separator == -1)
                    throw new Exception("invalid http header line: " + line);

                String name = line.Substring(0, separator);
                int pos = separator + 1;
                while ((pos < line.Length) && (line[pos] == ' '))
                {
                    pos++; // strip any spaces
                }

                string value = line.Substring(pos, line.Length - pos);
                Console.WriteLine("header: {0}:{1}", name, value);
                hHTTP_Headers[name] = value;
            }
        }

        private void OnHandleGETRequest()
        {
            hServer.OnGETRequest(this);
        }

        private void OnHandlePOSTRequest()
        {
            // this post data processing just reads everything into a memory stream.
            // this is fine for smallish things, but for large stuff we should really
            // hand an input stream to the request processor. However, the input stream 
            // we hand him needs to let him see the "end of the stream" at this content 
            // length, because otherwise he won't know when he's seen it all! 

            Console.WriteLine("get post data start");
            int content_len = 0;
            MemoryStream ms = new MemoryStream();

            if (this.hHTTP_Headers.ContainsKey("Content-Length"))
            {
                content_len = Convert.ToInt32(this.hHTTP_Headers["Content-Length"]);

                if (content_len > MAX_POST_SIZE)
                {
                    throw new Exception(String.Format("POST Content-Length({0}) too big for this simple server", content_len));
                }
                byte[] buf = new byte[BUF_SIZE];
                int to_read = content_len;

                while (to_read > 0)
                {
                    Console.WriteLine("starting Read, to_read={0}", to_read);

                    int numread = this.sInput.Read(buf, 0, Math.Min(BUF_SIZE, to_read));
                    Console.WriteLine("read finished, numread={0}", numread);
                    if (numread == 0)
                    {
                        if (to_read == 0)
                            break;
                        else
                            throw new Exception("client disconnected during post");
                    }
                    to_read -= numread;
                    ms.Write(buf, 0, numread);
                }
                ms.Seek(0, SeekOrigin.Begin);
            }
            Console.WriteLine("get post data end");
            hServer.OnPOSTRequest(this, new StreamReader(ms));

        }


        // Hilfs-Funktionen:
        // ---------------------------------------------------------------------------
        private string StreamReadLine(Stream inputStream)
        {
            int next_char;
            string data = "";

            while (true)
            {
                next_char = inputStream.ReadByte();
                if (next_char == '\n')
                    break;
                if (next_char == '\r')
                    continue;
                if (next_char == -1)
                {
                    Thread.Sleep(1);
                    continue;
                };

                data += Convert.ToChar(next_char);
            }

            return (data);
        }
    }

    public abstract class HttpServer
    {
        public HttpServer()
        {
            Stop();
        }


        // Eigenschaften:
        // --------------------------------------------------------------------------------------
        #region Properties
        [TypeConverter(typeof(CnvBooleanLabel.NoYes))]
        public bool IsActive
        {
            get { return bIsActive; }
        }
        #endregion


        // Events:
        // ---------------------------------------------------------------------------
        public abstract void OnGETRequest(HttpProcessor hProc);
        public abstract void OnPOSTRequest(HttpProcessor hProc, StreamReader sInputData);


        // Events:
        // ---------------------------------------------------------------------------
        public virtual void OnStart()
        {
        }
        public virtual void OnStop()
        {
        }


        // Management:
        // --------------------------------------------------------------------------------------
        public bool Start(int iPort)
        {
            if (bIsActive)
                return (false);
            else
            {
                this.bIsActive = true;
                this.iPort = iPort;

                tThread = new Thread(new ThreadStart(Listen));
                tThread.Start();

                OnStart();

                return (true);
            }
        }
        public void Stop()
        {
            if (tThread != null)
            {
                OnStop();

                // Listener beenden:
                tListener.Stop();
            }

            bIsActive = false;
            tListener = null;
            tThread = null;
        }


        // Management:
        // --------------------------------------------------------------------------------------
        protected void Listen()
        {
            tListener = new TcpListener(IPAddress.Any, iPort);

            tListener.Start();

            while (bIsActive)
            {
                try
                {
                    TcpClient tClient = tListener.AcceptTcpClient();

                    HttpProcessor hProcrocessor = new HttpProcessor(tClient, this);
                    Thread thread = new Thread(new ThreadStart(hProcrocessor.Process));

                    thread.Start();

                    Thread.Sleep(1);
                }
                catch (SocketException sExcpt)
                {
                }
            }
        }


        protected int iPort;


        private Thread tThread;
        private TcpListener tListener;
        private bool bIsActive;
    }
}



